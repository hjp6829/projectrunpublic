using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using ProjectRun.Framwork.Utility;
using UnityEngine.EventSystems;

[Serializable]
public class  AttackData
{
    public AttackBase attack;
    public bool isLastCombo;
    public string attackName;
    public float idleTime;
    public Dictionary<EAttackType, AttackData> NextAttack=new Dictionary<EAttackType, AttackData>();
}
public class CombatController : SerializedMonoBehaviour
{
    [SerializeField]
    private Dictionary<EAttackType, AttackData> attacks = new Dictionary<EAttackType, AttackData>();
    private AttackData nowAttackData;
    private EAttackType nowAttackType;
    private bool isCombo;
    private bool isLastAttack;

    public void SetComboData(Dictionary<EAttackType, AttackData> data)
    {
        attacks = data;
    }

    public bool SetAttack(EAttackType attacktype,bool value)
    {
        if (isLastAttack|| nowAttackData==null)
        {
            nowAttackData = attacks[attacktype];
            nowAttackType = attacktype;
            if (nowAttackType == EAttackType.X)
                isCombo = false;
            else
                isCombo = true;
            isLastAttack = false;
        }
        else
        {
            isLastAttack = false;
            if (nowAttackData.NextAttack.ContainsKey(attacktype))
            {
                nowAttackData = nowAttackData.NextAttack[attacktype];
                isCombo = true;
                return true;
            }
            isCombo = false;
            if (attacktype == EAttackType.X)
            {
                if (value)
                    return false;
                nowAttackData = attacks[EAttackType.X];
                return true;
            }
            else
            {
                isLastAttack = true;
                return false;
            }
        }
        return true;
    }
    public bool ComboCheck()
    {
        return isCombo;
    }
    public string GetNowAttackName()
    {
        return nowAttackData.attackName;//.attack.AttackName;
    }
    public float GetNowAttackIdleTime()
    {
        return nowAttackData.idleTime;//.attack.AttackName;
    }
    public bool CanAttackCheck()
    {
        return nowAttackData.attack.CanAttack();
    }
    public void StartAttack(Vector2 value, float angle, bool isflip,Actor actor)
    {
        nowAttackData.attack.StartAttack(value, angle, isflip, actor);
        if (nowAttackData.isLastCombo)
        {
            isLastAttack = true;
            //nowAttackData = null;
        }
    }
    public void EndAttackStart(Actor actor)
    {
        nowAttackData.attack?.SpineEndAttack(actor);
    }
    public void ShowGuideLine(Vector2 value, Vector2 dir,Actor actor)
    {
        nowAttackData.attack.ShowLine(value, dir, actor);
    }
    public void HideGuideLine()
    {
        nowAttackData.attack.HideLine();
    }
    public void ResetAttackData()
    {
        isLastAttack = true;
    }

    public void ResetAttackDataIfNotCombo()
    {
        if (!isCombo)
        {
            isLastAttack = true; 
        }
    }
}
