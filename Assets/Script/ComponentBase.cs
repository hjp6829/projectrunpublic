using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
public abstract class ComponentBase : SerializedMonoBehaviour
{
    private Rigidbody2D rb;
    private Collider2D collider2d;
    private Actor owner;

    protected Actor Owner { get
        {
            if(owner == null)
            {
                owner = GetComponent<Actor>();
                return owner;
            }
            return owner;
        } 
    }
    protected Rigidbody2D RB { get 
        { 
            if(rb==null)
            {
                rb = Owner.Rigidbody;
                return rb;
            }
            return rb;
        }
    }
    protected Collider2D GetCollider { get
        {
            if (collider2d == null)
            {
                collider2d = Owner.Collider2D;
                return collider2d;
            }
            return collider2d;
        } 
    }
    protected Coroutine activeCoroutine(Action action, float time = 0)
    {
        return StartCoroutine(Coroutineaction(action, time));
    }
    IEnumerator Coroutineaction(Action action,float time=0)
    {
        while(true)
        {
            time -= Time.deltaTime;
            if(time<=0)
            {
                action?.Invoke();
                break;
            }
            yield return null;
        }
    }
}
