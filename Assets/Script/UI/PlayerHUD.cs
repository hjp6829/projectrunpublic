using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
    private Player player;
    [SerializeField] private Image hpFilled;
    // Start is called before the first frame update
    void Start()
    {
        player = ServiceLocatorManager.GetService<GameManager>().Player;
        StatusCompoent statusComp = player.FindComponent<StatusCompoent>();
        statusComp.OnHPChange += (maxhp, hp, shiled) =>
        {
            hpFilled.fillAmount = hp / maxhp;
        };
    }
}
