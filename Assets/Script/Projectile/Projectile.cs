using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class Projectile : Actor
{
    [HideInInspector] public Action<float> OnDisableDistanceSettings;
    [SerializeField] protected int damage;
    [SerializeField] protected LayerMask mask;
    [SerializeField] protected bool isOwnerFlip;
    protected Actor actor;

    public abstract void initialization(Actor actor,bool isownerflip);
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        if (actor.gameObject.layer == collision.gameObject.layer)
            return;
        if (((1 << collision.gameObject.layer) & mask) != 0)
        {
            ExecuteEvents.Execute<IDamageable>(collision.gameObject, null, (a, b) => a.Hit(1, transform.position));
            Destroy(this.gameObject);
        }
    }
    protected virtual void Start()
    {
        ProjectileMove projectileMoveComp = FindComponent<ProjectileMove>();
        OnDisableDistanceSettings += (float dis) =>
        {
            projectileMoveComp.DisableDistance = dis;
        };
        projectileMoveComp.IsOwnerFlip=isOwnerFlip;
    }
}
