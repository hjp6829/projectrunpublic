using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LinePrediction : MonoBehaviour, ILinePredictionRenderer
{
    [SerializeField]
    private LineRenderer lineRenderer;
    public void Hide(float time)
    {
        StartCoroutine(Destory(time));
    }

    public void Show(Vector2 startPoint, Vector2 direction, float distance)
    {
        Vector2 EndPos = startPoint+direction.normalized*distance;
        lineRenderer.SetPosition(0, startPoint);
        lineRenderer.SetPosition(1, EndPos);
        lineRenderer.startWidth = 0.01f;
        lineRenderer.endWidth = 0.01f;
    }
    public void SetLineWidth(float startWidth,float endWidth)
    {
        lineRenderer.startWidth = startWidth;
        lineRenderer.endWidth = endWidth;
    }
    IEnumerator Destory(float time)
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }
}
