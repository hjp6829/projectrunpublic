using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.EventSystems;
using System.Net;

public class Bullet : Projectile
{
    public override void initialization(Actor actor, bool isownerflip)
    {
        this.actor = actor;
        isOwnerFlip = isownerflip;
    }
}
