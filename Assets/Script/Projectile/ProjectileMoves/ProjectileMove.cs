using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMove : ComponentBase
{
    [SerializeField] protected float speed;
    [SerializeField]
    [Tooltip("시작위치에서 투사체가 삭제되는 거리")]
    protected float disableDistance = 20.0f;
    protected Vector2 startPos;
    protected bool isOwnerFlip;

    public bool IsOwnerFlip { set => isOwnerFlip = value; }
    public float DisableDistance { set => disableDistance = value; }
    protected virtual void Awake()
    {
        startPos = transform.position;
        Owner.AddComponent(this);
    }
    protected virtual void FixedUpdate()
    {
        
    }
}
