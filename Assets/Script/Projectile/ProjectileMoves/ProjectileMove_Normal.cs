using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileMove_Normal : ProjectileMove
{
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        transform.Translate(Vector2.right * speed * Time.deltaTime);
        if (Vector2.Distance(startPos, transform.position) >= disableDistance)
            Destroy(this.gameObject);
    }
}
