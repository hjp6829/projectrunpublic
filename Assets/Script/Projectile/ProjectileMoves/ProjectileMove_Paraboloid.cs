using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using static Node;

public class ProjectileMove_Paraboloid : ProjectileMove
{
    [SerializeField] private float targetDis;
    private Vector2 startPosition;
    private Vector2 targetPosition;
    private float elapsedTime = 0.0f;
    private const float gravity = 9.8f;
    private Vector2 velocity;
    public float timeToTarget = 2.0f;

    private void Start()
    {
        startPosition = startPos;
        if (!isOwnerFlip)
            targetPosition = new Vector2(startPosition.x + targetDis, startPosition.y);
        else 
            targetPosition = new Vector2(startPosition.x - targetDis, startPosition.y);
        Debug.Log(targetPosition);
        Debug.Log(startPosition);
        velocity = (targetPosition - startPosition) / timeToTarget;
        velocity.y = (targetPosition.y - startPosition.y + 0.5f * gravity * Mathf.Pow(timeToTarget, 2)) / timeToTarget;
    }
    protected override void FixedUpdate()
    {
        base.FixedUpdate();
        elapsedTime += Time.deltaTime;
        float t = elapsedTime;
        Owner.transform.position = new Vector2(
            startPosition.x + velocity.x * t,
            startPosition.y + velocity.y * t - 0.5f * gravity * Mathf.Pow(t, 2));
    }
}
