using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMonsterArea : MonoBehaviour
{
    private List<Enemy> Enemys = new List<Enemy>();
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch(collision.gameObject.layer)
        {
            case 6:
                Enemys.Add(collision.GetComponent<Enemy>());
                break;
            case 7:
                foreach(var enemy in Enemys)
                {
                    if (enemy != null)
                        enemy.OnPlayerInArea?.Invoke(true);
                }
                break;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 7:
                foreach (var enemy in Enemys)
                {
                    if (enemy != null)
                        enemy.OnPlayerInArea?.Invoke(false);
                }
                break;
        }
    }
}
