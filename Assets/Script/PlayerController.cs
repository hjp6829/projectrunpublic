using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Interactions;
using System;
using Sirenix.OdinInspector;
using ProjectRun.Framwork.Utility;
public class PlayerController : SerializedMonoBehaviour, IService
{
    public static bool isInput;
    [HideInInspector]
    public Action<Vector2> OnMoveAxis;
    [HideInInspector]
    public Action<EMoveDir> OnInputDir;
    [HideInInspector]
    public Action OnInputGrbbing;
    [HideInInspector]
    public Action<bool> OnJumpAction;
    [HideInInspector]
    public Action<EAttackType> OnAttackAction;
    [HideInInspector]
    public Action OnDashAction;
    private PlayerInput input;
    private Vector2 MoveAxis;
    private EMoveDir Dir;
    private bool isAir;
    private bool isSit;
    private void Awake()
    {
        Register();
        isInput = true;
        input = new PlayerInput();
        input.Player.Enable();
        input.Player.AttackX.performed += Content =>
        {
            if (!isInput)
                return;
            OnAttackAction?.Invoke(EAttackType.X);
        };
        input.Player.AttackS.performed += Content =>
        {
            if (!isInput|| isAir|| isSit)
                return;
            OnAttackAction?.Invoke(EAttackType.S);
        };
        input.Player.AttackA.performed += Content =>
        {
            if (!isInput|| isAir|| isSit)
                return;
            OnAttackAction?.Invoke(EAttackType.A);
        };
        //input.Player.SpecialAttack.performed += Content =>
        //{
        //    if (!isInput)
        //        return;
        //    if (Content.interaction is HoldInteraction)
        //        OnSpeacialHoldAction?.Invoke();
        //    else if (Content.interaction is TapInteraction)
        //        OnSpeacialAction?.Invoke();
        //};
        input.Player.Jump.performed += Content =>
        {
            if (!isInput)
                return;
            if (Content.interaction is HoldInteraction)
                OnJumpAction?.Invoke(true);
            else if (Content.interaction is TapInteraction)
                OnJumpAction?.Invoke(false);
        };
        input.Player.Dash.performed += Content =>
        {
            if (!isInput)
                return;
            OnDashAction?.Invoke();
        };
        input.Player.Move.performed += Content =>
        {
            MoveAxis = Content.ReadValue<Vector2>();

            ///axis값이 위에꺼랑 같이 누르면 0.72로 떨어져서 넣었음
            //if (MoveAxis.x < 0)
            //    MoveAxis.x = -1;
            //else if (MoveAxis.x > 0)
            //    MoveAxis.x = 1;
            if (MoveAxis.x < 0)
                Dir = EMoveDir.LEFT;
            else if (MoveAxis.x > 0)
                Dir = EMoveDir.RIGHT;
            else if (MoveAxis.y < 0)
                Dir = EMoveDir.DOWN;
            else if (MoveAxis.y > 0)
                Dir = EMoveDir.UP;
            OnInputDir?.Invoke(Dir);
            OnInputGrbbing?.Invoke();
        };
        input.Player.Move.canceled += Content =>
        {
            MoveAxis = Content.ReadValue<Vector2>();
            Dir = EMoveDir.NONE;
            OnInputDir?.Invoke(Dir);
        };
    }
    private void Start()
    {
        Player player = ServiceLocatorManager.GetService<GameManager>().Player;
        PlayerMoveComponent moveComp = player.FindComponent<PlayerMoveComponent>();
        moveComp.OnAir += (bool value) =>
        {
            isAir = value;
        };
        moveComp.OnSit += (bool value) =>
        {
            isSit = value;
        };
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    public void FixedUpdate()
    {
        OnMoveAxis?.Invoke(MoveAxis);
    }
}
