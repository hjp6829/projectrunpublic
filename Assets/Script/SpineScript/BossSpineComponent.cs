using Spine;
using System;
using UnityEngine;

public class BossSpineComponent : SpineComponent
{
    [HideInInspector] public Action<Actor> OnAttackEndStart;
    private string nowAttackName;
    protected override void Start()
    {
        base.Start();
    }
    public override void OnSpineStart(TrackEntry trackEntry)
    {
        base.OnSpineStart(trackEntry);
        string name = trackEntry.Animation.Name;
        if (name.Contains("Attack"))
        {
            string[] parts = name.Split('_');
            switch (parts[2])
            {
                case "Idle":
                    DoEvent();
                    break;
                case "Start":
                    OnAttackStart?.Invoke();
                    break;
                case "End":
                    OnAttackEndStart?.Invoke(Owner);
                    break;
            }
            nowAttackName = parts[1];
        }
    }
    public override void OnSpineComplete(TrackEntry trackEntry)
    {
        base.OnSpineComplete(trackEntry);
        string name = trackEntry.Animation.Name;
        if (name.Contains("Attack"))
        {
            OnAttackStart?.Invoke();
            string[] parts = name.Split('_');
            switch (parts[2])
            {
                case "End":
                    OnAttackComplite?.Invoke();
                    break;
                case "Start":
                    PlayAttackIdle();
                    activeCoroutine(() =>
                    {
                        PlayAttackEnd();
                    },attackIdleTime);
                    break;
            }
        }
    }
    public void PlayAttackIdle()
    {
        SetSpineAnim("Attack_" + nowAttackName + "_Idle", true);
    }
    public void PlayAttackEnd()
    {
        SetSpineAnim("Attack_" + nowAttackName + "_End", true);
    }
}
