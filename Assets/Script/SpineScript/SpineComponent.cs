using Spine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Spine.Unity;
using System;
using ProjectRun.Framwork.Utility;
public class SpineComponent : ComponentBase, ISpineCallback
{
    public float testSpineTime = 1;
    #region SpineEvents
    [HideInInspector] public Action<string> OnSpineName;
    [HideInInspector] public Action OnAttackStart;
    [HideInInspector] public Action OnAttackComplite;
    [HideInInspector] public Action OnAttackEnd;
    [HideInInspector] public Action<Vector2,float,bool,Actor> OnAttackDo;
    [HideInInspector] public Action OnAttackCombo;
    [HideInInspector] public Action OnJumpComplite;
    [HideInInspector] public Action OnJumpStart;
    [HideInInspector] public Action OnDashComplite;
    [HideInInspector] public Action OnDashStarte;
    [HideInInspector] public Action OnHitStart;
    [HideInInspector] public Action OnHitComplite;
    [HideInInspector] public Action OnHangingStart;
    [HideInInspector] public Action OnClimbComplite;
    [HideInInspector] public Action<Vector2,Vector2, Actor> OnShowLine;
    [HideInInspector] public Action OnHideLine;
    [HideInInspector] public Action OnGroggy;
    [HideInInspector] public Action OnGroggyComplite;
    #endregion
    [SerializeField]
    [BoxGroup("Renderer")]
    protected SkeletonAnimation _SkeletonAnimation;
    protected bool isCombo;
    protected string NowSpineName;
    protected float attackIdleTime;

    private SpineCoordinator spineCoordinator;
    private float localScaleX;
    private string prefix;

    public SpineCoordinator SpineCoordinator
    {
        get
        {
            if (spineCoordinator != null)
            {
                return spineCoordinator;
            }
            if (_SkeletonAnimation == null)
            {
                Debug.LogError($"{ToString()}은 스파인이 아닙니다.");
            }
            spineCoordinator = new SpineCoordinator(this, _SkeletonAnimation);
            spineCoordinator.Initialize();
            if (spineCoordinator.CanLoad())
            {
                spineCoordinator.Load();
            }

            return spineCoordinator;
        }
    }
    private void Awake()
    {
        Owner.AddComponent(this);
    }
    protected virtual void Start()
    {
        SpineCoordinator.TimeScale = testSpineTime;
        localScaleX = Owner.GetBody().transform.localScale.x;
        if (Owner.ContainComponent<AttackComponent>())
        {
            AttackComponent attackComp = Owner.FindComponent<AttackComponent>();
            attackComp.OnAttackNameTrack01 += (string name, bool value) =>
            {
                SetSpineAnimTrack1(name, value);
            };
            attackComp.OnAttackNameTrack02 += (string name, bool value) =>
            {
                SetSpineAnim(name, value);
            };
            attackComp.OnCombo += () =>
            {
                isCombo = true;
            };
            attackComp.OnSetIdleTime += (float time) =>
            {
                attackIdleTime = time;
            };
        }
        if (Owner.ContainComponent<MoveComponent>())
        {
            MoveComponent moveComp = Owner.FindComponent<MoveComponent>();
            moveComp.OnMoveAction += (string name, bool value) =>
            {
                SetSpineAnim(name, value);
            };
            moveComp.OnFlip += (bool value) =>
            {
                SetFlip(value);
            };
        }
        if (Owner.ContainComponent<StatusCompoent>())
        {
            StatusCompoent statusComp = Owner.FindComponent<StatusCompoent>();
            statusComp.OnStatusName += (string name, bool value) =>
            {
                SetSpineAnim(name, value);
            };
        }
    }
    public void AddSpine(string name, bool value)
    {
        SpineCoordinator.Add(name, value);
    }
    public void AddSpinetest(string name, bool value)
    {
        SpineCoordinator.Addtest(name, value);
    }
    public virtual void OnSpineComplete(TrackEntry trackEntry)
    {
        string name = trackEntry.Animation.Name;
        switch (name)
        {
            case "Hit":
                OnHitComplite?.Invoke();
                break;
            case "Groggy":
                OnGroggyComplite?.Invoke();
                break;
        }
    }

    public virtual void OnSpineEnd(TrackEntry trackEntry)
    {
        
    }

    public virtual void OnSpineEvent(TrackEntry trackEntry, string eventName)
    {
        string name = trackEntry.Animation.Name;

        if(name.Contains("Attack"))
        {
            switch (eventName)
            {
                case "Do":
                    DoEvent();
                    OnHideLine?.Invoke();
                    break;
                case "Combo":
                    ComboEvent();
                    break;
                case "Show":
                    ShowLineEvent();
                    break;
            }
        }
    }
    protected void ComboEvent()
    {
        OnAttackCombo?.Invoke();
        isCombo = true;
    }
    protected void DoEvent()
    {
        OnHideLine?.Invoke();
        Bone fireposbone = _SkeletonAnimation.skeleton.FindBone("FirePos");
        float X = fireposbone.WorldX;
        float angle = fireposbone.Rotation;
        if (IsFlip())
        {
            angle = 180.0f - angle;
            X *= -1;
        }
        Vector2 FirePos = new Vector2(X, fireposbone.WorldY);
        OnAttackDo?.Invoke(Owner.transform.TransformPoint(FirePos), angle, IsFlip(), Owner);
    }
    private void ShowLineEvent()
    {
        Bone fireposbone = _SkeletonAnimation.skeleton.FindBone("FirePos");
        float X = fireposbone.WorldX;
        Vector2 direction=Vector2.right;
        if (IsFlip())
        {
            direction = Vector2.left;
            X *= -1;
        }
        Vector2 FirePos = new Vector2(X, fireposbone.WorldY);
        OnShowLine?.Invoke(Owner.transform.TransformPoint(FirePos), direction, Owner);
    }
    private bool IsFlip()
    {
        if (Owner.GetBody().transform.localScale.x == -1)
            return true;
        return false;
    }
    public virtual void OnSpineStart(TrackEntry trackEntry)
    {
        //Debug.Log(trackEntry +" / "+Owner.name);
        string name = trackEntry.Animation.Name;
        switch (name)
        {
            case "Hit":
                OnHitStart?.Invoke();
                break;
            case "Groggy":
                OnGroggy?.Invoke();
                break;
        }
    }

    public void SetSkin(string name)
    {
        throw new System.NotImplementedException();
    }

    public void SetSpine(SpineAsset asset)
    {
        SpineCoordinator.LoadSpine = asset;
        SpineCoordinator.Initialize();
        SpineCoordinator.Load();
    }

    public virtual bool SetSpineAnim(string name, bool value)
    {
        name = name + prefix;
        if (NowSpineName == name)
            return true;
        OnSpineName?.Invoke(name);
        NowSpineName = name;
        SpineCoordinator.Set(name, value);
        return true;
    }
    public virtual bool SetSpineAnimTrack1(string name, bool value)
    {
        SpineCoordinator.SetTrack1(name, value);
        return true;
    }
    public void SetSpineTimeScale(float value)
    {
        throw new System.NotImplementedException();
    }
    public virtual void SetFlip(bool value)
    {
        var localscale = Owner.GetBody().transform.localScale;
        if (value)
            localscale.x = -localScaleX;
        else
            localscale.x = localScaleX;
        Owner.GetBody().transform.localScale = localscale;
    }
    public void SetPrefix(string prefix)
    {
        this.prefix = prefix;
    }
}
