using Spine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpineComponent : SpineComponent
{
    [HideInInspector] public Action OnNormalUpStart;
    [HideInInspector] public Action OnNormalUpComplite;
    [HideInInspector] public Action OnComboComplite;
    [HideInInspector] public Action OnComboStart;


    public override void OnSpineStart(TrackEntry trackEntry)
    {
        base.OnSpineStart(trackEntry);
        string name = trackEntry.Animation.Name;
        if (name.Contains("Combo"))
        {
            OnComboStart?.Invoke();
            OnAttackStart?.Invoke();
        }
        else if (name.Contains("Attack"))
        {
            OnAttackStart?.Invoke();
        }
        switch (name)
        {
            case "Dash":
                OnDashStarte?.Invoke();
                break;
            case "Pistol_Idle_Down":
            case "Pistol_Move_Down":
                OnNormalUpComplite?.Invoke();
                OnAttackStart?.Invoke();
                break;
            case "Psitol_Move_Ready":
            case "Psitol_Idle_Ready":
                OnNormalUpStart?.Invoke();
                break;
            case "Hanging":
                OnHangingStart?.Invoke();
                break;
            case "Jump":
                OnJumpStart?.Invoke();
                break;
        }
    }
    public override void OnSpineEvent(TrackEntry trackEntry, string eventName)
    {
        base.OnSpineEvent(trackEntry, eventName);
        string name = trackEntry.Animation.Name;
        if (name == "Pistol_Move_Down" || name == "Pistol_Idle_Down")
        {
            switch (eventName)
            {
                case "Do":
                    DoEvent();
                    break;
                case "Combo":
                    ComboEvent();
                    break;
            }
        }
    }
    public override void OnSpineComplete(TrackEntry trackEntry)
    {
        base.OnSpineComplete(trackEntry);
        string name = trackEntry.Animation.Name;
        if (isCombo)
        {
            if (name.Contains("Combo") && name != NowSpineName)
                return;
            else if (name.Contains("Combo") && name == NowSpineName)
                isCombo = false;
        }
        if (name.Contains("Combo"))
        {
            OnComboComplite?.Invoke();
        }
        switch (name)
        {
            case "Dash":
                OnDashComplite?.Invoke();
                break;
            case "Climb":
                OnClimbComplite?.Invoke();
                break;
            case "Pistol_Move_Down":
                _SkeletonAnimation.AnimationState.AddEmptyAnimation(1, 0.1f, _SkeletonAnimation.AnimationState.GetCurrent(1).Animation.Duration);
                OnAttackComplite?.Invoke();
                break;
            case "Pistol_Move_Ready":
                SetSpineAnimTrack1("Pistol_Move" + "_Down", false);
                break;
            case "Pistol_Idle_Down":
                _SkeletonAnimation.AnimationState.AddEmptyAnimation(1, 0.1f, _SkeletonAnimation.AnimationState.GetCurrent(1).Animation.Duration);
                OnAttackComplite?.Invoke();
                break;
            case "Pistol_Idle_Ready":
                SetSpineAnimTrack1("Pistol_Idle" + "_Down", false);
                break;
        }
    }
    public override void OnSpineEnd(TrackEntry trackEntry)
    {
        base.OnSpineEnd(trackEntry);
        string name = trackEntry.Animation.Name;
        switch (name)
        {
            case "Dash":
                OnDashComplite?.Invoke();
                break;
        }
    }
}
