using Spine;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpineComponent : SpineComponent
{
    [SerializeField] private float attackEndDelay;
    protected override void Start()
    {
        base.Start();
        Owner.FindComponent<StatusCompoent>().OnHit += () =>
        {
            SetSpineAnim("Hit", false);
        };
    }
    public override void OnSpineStart(TrackEntry trackEntry)
    {
        base.OnSpineStart(trackEntry);
        string name = trackEntry.Animation.Name;
        if (name.Contains("Attack"))
        {
            OnAttackStart?.Invoke();
            switch (name) 
            {
                case "Do":
                    DoEvent();
                    break;
            }
        }
        switch (name)
        {
            case "Shield_Broken":
                OnHitStart?.Invoke();
                break;
        }
    }
    public override void OnSpineComplete(TrackEntry trackEntry)
    {
        base.OnSpineComplete(trackEntry);
        string name = trackEntry.Animation.Name;
        if (name.Contains("Attack"))
        {
            activeCoroutine(() =>
            {
                OnAttackComplite?.Invoke();
            }, attackIdleTime);
        }
        switch (name)
        {
            case "Shield_Broken":
                SetSpineAnim("Groggy", false);
                //OnHitComplite?.Invoke();
                break;
        }
    }
}
