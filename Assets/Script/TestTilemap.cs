using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class TestTilemap : Tile
{
    public string additionalInfo; // 원하는 데이터를 저장하는 속성을 추가

    public override bool StartUp(Vector3Int position, ITilemap tilemap, GameObject go)
    {
        // 별도의 초기화 코드를 작성할 수 있습니다.

        // 부모 클래스의 StartUp 함수를 호출하여 정상 작동을 보장합니다.
        return base.StartUp(position, tilemap, go);
    }
}
