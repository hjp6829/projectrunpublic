using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using ProjectRun.Framwork.Utility;
using System;

public class AttackComponent : ComponentBase
{
    [HideInInspector]
    public Action<string, bool> OnAttackNameTrack01;
    [HideInInspector]
    public Action<string, bool> OnAttackNameTrack02;
    [HideInInspector]
    public Action OnCombo;
    [HideInInspector] public Action<float> OnSetIdleTime;

    protected float idleTime;

    private void Awake()
    {
        Owner.AddComponent(this);
    }
    protected virtual void Start()
    {  

    }
    /// <summary>
    /// 2023.6.12일
    /// Actor에서 일반공격 실행
    /// </summary>
    public virtual void Attack(EAttackType attackType) {}

}
