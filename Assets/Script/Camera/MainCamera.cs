using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera : MonoBehaviour
{
    private Transform Target;
    private float leftRange;
    private float rightRange;
    private float UpRange;
    private float DownRange;
    private Camera mainCamera;
    private void Start()
    {
        Target = ServiceLocatorManager.GetService<GameManager>().Player.transform;
        mainCamera = GetComponent<Camera>();
        float cameraSize = mainCamera.orthographicSize;
        float aspectRatio = mainCamera.aspect;

        //float xPosition = cameraSize * aspectRatio;
        //Vector2 boundaryBoxPosition = new Vector2(
        //    mainCamera.ScreenToWorldPoint(new Vector2(xPosition, 0)).x - BoundaryBox.size.x * 0.5f,
        //    BoundaryBox.transform.position.y
        //);
        //BoundaryBox.transform.position = boundaryBoxPosition;
    }
    private void FixedUpdate()
    {
        LimitCameraPosition();
    }
    public void SetAreaSize(Vector2 center,Vector2 size)
    {
        leftRange = center.x - (size.x*0.5f);
        rightRange = center.x + (size.x * 0.5f);
        DownRange = center.y - (size.y * 0.5f);
        UpRange = center.y + (size.y * 0.5f);
    }
    private void LimitCameraPosition()
    {
        float camHalfWidth = mainCamera.orthographicSize * mainCamera.aspect;
        float camHalfHeight = mainCamera.orthographicSize;
        float minX = leftRange + camHalfWidth;
        float maxX = rightRange - camHalfWidth;
        float minY = DownRange + camHalfHeight;
        float maxY = UpRange - camHalfHeight;

        float x = Mathf.Clamp(Target.transform.position.x, minX, maxX);
        float y = Mathf.Clamp(Target.transform.position.y, minY, maxY);

        mainCamera.transform.position = new Vector3(x, y, mainCamera.transform.position.z);
    }
}
