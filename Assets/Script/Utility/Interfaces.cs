using UnityEngine.EventSystems;
using UnityEngine;
public interface ISpineCallback
{
    void OnSpineStart(Spine.TrackEntry trackEntry);
    void OnSpineComplete(Spine.TrackEntry trackEntry);
    void OnSpineEnd(Spine.TrackEntry trackEntry);
    void OnSpineEvent(Spine.TrackEntry trackEntry, string eventName);
    void SetSpine(SpineAsset asset);
    bool SetSpineAnim(string name, bool value);
    void SetSpineTimeScale(float value);
    void SetSkin(string name);
    void AddSpine(string name, bool value);
}
public interface IDamageable: IEventSystemHandler
{
    void Hit(int damage, Vector3 hitPos);
}
public interface ILinePredictionRenderer: IEventSystemHandler
{
    void Show(Vector2 startPoint, Vector2 direction, float distance);
    void Hide(float time);
}