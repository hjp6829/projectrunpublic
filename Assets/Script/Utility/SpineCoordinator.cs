using System.Linq;
using Spine.Unity;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
public struct SpineAsset
{
    public SkeletonDataAsset Asset;
    [ValueDropdown("SkinList")]
    public string Skin;
    [ValueDropdown("AnimationList")]
    public string Animation;
    public SpineAsset(SkeletonDataAsset asset, string skinName = null, string animationName = null)
    {
        Asset = asset;
        Skin = skinName;
        Animation = animationName;

        if (Asset != null)
        {
            FillInNull();
        }
    }
    public List<string> SkinList
    {
        get
        {
            if (Asset == null)
            {
                return new List<string>();
            }

            List<string> temp = new List<string>();

            var skins = Asset.GetSkeletonData(false).Skins;

            for (int i = 0; i < skins.Count; i++)
            {
                temp.Add(skins.ElementAt(i).Name);
            }

            return temp;
        }
    }
    public List<string> AnimationList
    {
        get
        {
            if (Asset == null)
            {
                return new List<string>();
            }
            var jsonData = (Dictionary<string, object>)SharpJson.JsonDecoder.DecodeText(Asset.skeletonJSON.ToString());
            return ((Dictionary<string, object>)jsonData["animations"]).Keys.ToList();
        }
    }
    public float Duration
    {
        get
        {
            if (Asset == null)
            {
                Debug.LogError("파일이 없음");
            }
            var index = AnimationIndexOf(Animation);
            if (index == -1)
            {
                throw new IndexOutOfRangeException($"{Asset.name}에서 애니메이션[{Animation}]으로 인덱스를 구할 수 없었습니다.");
            }
            return Asset.duration[index];
        }
    }
    public int SkinIndexOf(string skinName)
    {
        if (Asset == null)
        {
            Debug.LogError("Asset 이 Null 일때 실행 할 수 없습니다.");
        }
        return SkinList.IndexOf(skinName);
    }
    /// <summary>
    /// @ 애니메이션 인덱스 - 애니메이션 인덱스를 찾아 얻습니다.
    /// </summary>
    /// <param name="animationName">찾을 애니메이션 이름입니다.</param>
    /// <returns>없다면 -1, 있으면 해당 인덱스를 불러 옵니다.</returns>
    public int AnimationIndexOf(string animationName)
    {
        if (Asset == null)
        {
            Debug.LogError("Asset 이 Null 일때 실행 할 수 없습니다.");
        }
        return AnimationList.IndexOf(animationName);
    }
    /// <summary>
    /// 비어있나? - 에셋, 스킨, 애니메이션 3가지 전부 하나라도 비어있을때 입니다.
    /// </summary>
    public bool IsEmpty() => Asset == null || Skin == null || Animation == null;
    public override string ToString() => $"{Asset?.name}({Skin}) => [{Animation}]";
    /// <summary>
    /// 빈값채우기 - Skin, Animation 빈값을 <c>Newtro.Framework.SpineAsset.Asset</c>을 기반으로 채웁니다.
    /// </summary>
    /// <remarks>
    /// Skin, Animation 둘 다 첫번째 항목으로 채웁니다.
    /// </remarks>
    public void FillInNull()
    {
        if (Asset != null)
        {
            if (Skin == null)
            {
                // 스킨이 디폴트 스킨밖에 없다면, 디폴트 스킨으로
                if (SkinList.Count == 1)
                {
                    Skin = SkinList.ElementAt(0);
                }
                // 스킨이 1개 이상 있다면 제일 첫번째 스킨으로
                else if (SkinList.Count > 1)
                {
                    Skin = SkinList.ElementAt(1);
                }
            }

            if (Animation == null)
            {
                Animation = AnimationList.ElementAt(0);
            }
        }
    }
    /// <summary>
    /// 비우기 - 모든값을 Null 만듭니다.
    /// </summary>
    public void Empty()
    {
        Asset = null;
        Skin = null;
        Animation = null;
    }
}
public class SpineCoordinator
{
    private ISpineCallback _owner;
    private SkeletonAnimation _skeletonAnimation;
    private MeshRenderer _meshRenderer;
    private SpineAsset _loadSpine;
    public bool Loop
    {
        get
        {
            if (_skeletonAnimation)
            {
                return _skeletonAnimation.loop;
            }
            else
            {
                return false;
            }
        }

        set
        {
            if (_skeletonAnimation)
            {
                _skeletonAnimation.loop = value;
            }
        }
    }
    public SpineAsset LoadSpine { get => _loadSpine; set => _loadSpine = value; }
    public SpineAsset CurrentAsset
    {
        get
        {
            if (_skeletonAnimation.skeletonDataAsset == null)
            {
                return new SpineAsset();
            }
            return new SpineAsset(
                _skeletonAnimation?.skeletonDataAsset ?? null,
                _skeletonAnimation?.skeleton?.Skin?.Name ?? null,
                _skeletonAnimation?.AnimationName ?? null);
        }
    }
    public Transform Transform => _skeletonAnimation.transform;
    public float Time { get => TrackEntry.TrackTime; set => TrackEntry.TrackTime = value; }
    public float TimeScale { get; set; } = 1.0f;
    public int Order { get => _meshRenderer.sortingOrder; }
    public string SortingLayer { get => _meshRenderer.sortingLayerName; set => _meshRenderer.sortingLayerName = value; }
    private global::Spine.TrackEntry TrackEntry => _skeletonAnimation.state.Tracks.ElementAt(0);
    private Material _originalMaterial;
    public SpineCoordinator(ISpineCallback owner, SkeletonAnimation skeletonAnimation)
    {
        this._owner = owner;
        _skeletonAnimation = skeletonAnimation;
        _meshRenderer = _skeletonAnimation.GetComponent<MeshRenderer>();
        var skinName = _skeletonAnimation.initialSkinName;
        _loadSpine = new SpineAsset(
            _skeletonAnimation.skeletonDataAsset,
            _skeletonAnimation.initialSkinName == "" ? "default" : skinName,
            _skeletonAnimation.AnimationName);
    }
    public void SetSkin(string skinName)
    {
        if (IsLoad())
        {
            var foundSkin = CurrentAsset.SkinList.Find((skin) => skin == skinName);
            if (foundSkin != null)
            {
                _skeletonAnimation.skeleton.SetSkin(foundSkin);
            }
            else
            {
                throw new KeyNotFoundException($"{ToString()}은 {_skeletonAnimation.skeletonDataAsset.name} 에서 {skinName} 이라는 스킨을 찾지 못했습니다.");
            }
        }
    }
    public bool CanLoad() => _skeletonAnimation != null && !_loadSpine.IsEmpty();
    private bool IsLoad() => _skeletonAnimation != null || _skeletonAnimation.state != null && _meshRenderer != null;
    public void Load()
    {
        if (_skeletonAnimation == null)
        {
            Debug.LogError("초기화 하기 위해 SkeletonAnimation을 설정 해야합니다.");
        }
        if (_loadSpine.Asset == null)
        {
            Debug.LogError("초기화 하기 위해 InitialSpine을 설정 해야합니다.");
        }

        _loadSpine.FillInNull();

        _skeletonAnimation.initialSkinName = _loadSpine.Skin;
        _skeletonAnimation.skeletonDataAsset = _loadSpine.Asset;

        _skeletonAnimation.Initialize(true);
        _meshRenderer = _skeletonAnimation.GetComponent<MeshRenderer>();

        _skeletonAnimation.state.Start += Start;
        _skeletonAnimation.state.Complete += Complete;
        _skeletonAnimation.state.End += End;
        _skeletonAnimation.state.Event += Event;

        _originalMaterial = _meshRenderer.material;
        if (_loadSpine.Animation != "")
            Set(_loadSpine.Animation);
    }
    private void Start(Spine.TrackEntry trackEntry)
    {
        _owner.OnSpineStart(trackEntry);
    }
    private void Complete(Spine.TrackEntry trackEntry)
    {
        _owner.OnSpineComplete(trackEntry);
    }
    private void End(Spine.TrackEntry trackEntry)
    {
        _owner.OnSpineEnd(trackEntry);
    }
    private void Event(Spine.TrackEntry trackEntry, Spine.Event @event)
    {
        _owner.OnSpineEvent(trackEntry, @event.ToString());
    }
    /// <summary>
    /// 초기화 - 스파인을 초기화 합니다.
    /// </summary>
    /// <exception cref="CannotInitializeException">
    /// SkeletonAnimation과 InitialSpine을 지정해야 합니다.
    /// </exception>
    public void Initialize()
    {
        _skeletonAnimation.ClearState();
    }
    public void Play()
    {
        //Show();
        _skeletonAnimation.timeScale = TimeScale;
    }
    public void Pause()
    {
        _skeletonAnimation.timeScale = 0f;
    }
    /// <summary>
    /// 정하기 - 기본적으로 가진 스파인의 애니메이션을 설정합니다.
    /// </summary>
    public void Set(string animationName, bool? loop = null)
    {
        if (IsLoad())
        {
            try
            {
                if (loop.HasValue)
                {
                    _skeletonAnimation.loop = loop.Value;
                }
                _skeletonAnimation.state.SetAnimation(0, animationName, _skeletonAnimation.loop);
                Play();
            }
            catch (ArgumentException)
            {
                Pause();
                Debug.LogError("스파인 없음 : " + animationName);
            }
        }
    }
    public void SetTrack1(string animationName, bool? loop = null)
    {
        if (IsLoad())
        {
            try
            {
                if (loop.HasValue)
                {
                    _skeletonAnimation.loop = loop.Value;
                }
                _skeletonAnimation.state.SetAnimation(1, animationName, _skeletonAnimation.loop);
                Play();
            }
            catch (ArgumentException)
            {
                Pause();
                Debug.LogError("스파인 없음 : " + animationName);
            }
        }
    }
    /// <summary>
    /// 추가하기 - 현재 실행하고 있는 애니메이션 다음에 실행할 애니메이션 입니다.
    /// </summary>
    public void Add(string animationName, bool loop, float delay = 0.0f)
    {
        if (IsLoad())
        {
            _skeletonAnimation.state.AddAnimation(0, animationName, loop, delay);
        }
    }
    public void Addtest(string animationName, bool loop, float delay = 0.0f)
    {
        if (IsLoad())
        {
            _skeletonAnimation.state.AddAnimation(1, animationName, loop, delay);
        }
    }
    //public void SortOrder()
    //{
    //    if (IsLoad())
    //    {
    //        _meshRenderer.sortingOrder = (int)(_meshRenderer.transform.position.z * -100);
    //    }
    //}
    public void MaskingMaterial(Material mask)
    {
        if (IsLoad())
        {
            if (!_skeletonAnimation.CustomMaterialOverride.ContainsKey(_originalMaterial))
                _skeletonAnimation.CustomMaterialOverride.Add(_originalMaterial, mask);
        }
    }
    public void UnmaskingMaterial()
    {
        if (IsLoad())
        {
            _skeletonAnimation.CustomMaterialOverride.Clear();
        }
    }
    #region 컬링부분
    //public void Hide()
    //{
    //    if (IsLoad())
    //    {
    //        if (_meshRenderer.enabled)
    //        {
    //            _meshRenderer.enabled = false;
    //            //if (actortype == ActorType.MONSTER && GameMode.ShowMonsters.ContainsKey(parent))
    //            //{
    //            //    //UnityEngine.Debug.Log("사라짐 : " + testname + "/" + GameMode.MosterIDX);
    //            //    GameMode.ShowMonsters.Remove(parent);
    //            //}
    //        }
    //    }
    //}
    //private void CollingShow()
    //{
    //    if (IsLoad())
    //    {
    //        _meshRenderer.enabled = true;
    //        //if (actortype == ActorType.MONSTER && !GameMode.ShowMonsters.ContainsKey(ActorNumer))
    //        //{
    //        //    GameMode.ShowMonsters.Add(ActorNumer, true);
    //        //}
    //    }
    //}
    //public void Show()
    //{
    //    if (IsLoad())
    //    {
    //        _meshRenderer.enabled = true;
    //    }
    //}
    //public bool? Culling()
    //{
    //    var cam = GameMode.Instance.CameraManager.Playercamera;

    //    if (cam != null)
    //    {
    //        Vector2 center = _meshRenderer.bounds.center;
    //        Vector2 size = _meshRenderer.bounds.size * 0.5f;

    //        var min = cam.WorldToViewportPoint(center - size);
    //        var max = cam.WorldToViewportPoint(center + size);
    //        //if (( min.x > 0.1f&& min.x<0.9f) && (min.y < 0.9f&&min.y>0.1f))
    //        //{
    //        //    if (_meshRenderer.enabled)
    //        //    {
    //        //        //if (actortype == ActorType.MONSTER && !GameMode.ShowMonsters.ContainsKey(actor))
    //        //        //{
    //        //        //    GameMode.ShowMonsters.Add(actor, true);
    //        //        //}
    //        //    }
    //        //}

    //        if (max.x < 0 || min.x > 1 ||
    //        max.y < 0 || min.y > 1)
    //        {
    //            Hide();
    //            return true;
    //        }
    //        else
    //        {
    //            CollingShow();
    //            return false;
    //        }
    //    }
    //    return null;
    //}
    #endregion
    public string GetAnimationName()
    {
        return _skeletonAnimation.AnimationName;
    }

    public Spine.Animation[] GetAnimationList()
    {
        SkeletonDataAsset skeletonDataAsset = _skeletonAnimation.SkeletonDataAsset;
        Spine.Animation[] animations = skeletonDataAsset.GetAnimationStateData().SkeletonData.Animations.ToArray();

        return animations;
    }
}