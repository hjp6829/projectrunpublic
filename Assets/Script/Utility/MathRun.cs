using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ProjectRun.Framwork.Utility
{
    public static class MathRun
    {
        public static List<T> GetShuffleList<T>(List<T> _list)
        {

            for (int i = _list.Count - 1; i > 0; i--)
            {
                int rnd = UnityEngine.Random.Range(0, i);

                T temp = _list[i];
                _list[i] = _list[rnd];
                _list[rnd] = temp;
            }

            return _list;
        }
        public static Vector2 GetAngleToVector2(float angle)
        {
            float angleInRadians = angle * Mathf.Deg2Rad;
            Vector2 direction = new Vector2(Mathf.Cos(angleInRadians), Mathf.Sin(angleInRadians));
            return direction;
        }
    }
}

