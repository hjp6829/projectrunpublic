namespace ProjectRun.Framwork.Utility
{
    public enum EAttackType { X,A,S,B,D,E,F,C }
    public enum EMoveDir { NONE,RIGHT,LEFT,UP,DOWN}
    public enum EOneWayDir { LEFT,RIGHT }
    public enum EWalkState { NONE,JUMPING,DASHING,FALLING }
    public enum EJumpType { NONE,ONE,SECOND }
}
