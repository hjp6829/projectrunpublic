using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
[Serializable]
public class AngleAttackData
{
    public int BulletCount=1;
    public float DelayTime=0.5f;
    [Range(-180, 180)] public float angle;
}
public class AngleBasedAttack : AttackBase
{
    [SerializeField] private GameObject bullet;
    [SerializeField]
    private List<AngleAttackData> angleAttackDatas = new List<AngleAttackData>();
    public override bool StartAttack(Vector2 value, float angle, bool isflip, Actor actor)
    {
        if (!base.StartAttack(value, angle, isflip, actor))
            return false;
        foreach(AngleAttackData data in angleAttackDatas)
        {
            StartCoroutine(FireBullets(data, value, actor, isflip));
        }
        return true;
    }
    private IEnumerator FireBullets(AngleAttackData data,Vector2 value,Actor actor,bool isflip)
    {
        for (int i = 0; i < data.BulletCount; i++)
        {
            Bullet temp = Instantiate(bullet, value, Quaternion.Euler(0, 0, data.angle)).GetComponent<Bullet>();
            temp.initialization(actor, isflip);
            yield return new WaitForSeconds(data.DelayTime);
        }
    }
}
