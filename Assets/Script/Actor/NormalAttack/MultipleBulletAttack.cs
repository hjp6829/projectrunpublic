using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
using Sirenix.OdinInspector;
public class MultipleBulletAttack : AttackBase
{
    [SerializeField] private GameObject bullet;
    [SerializeField] private int bulletNumber;
    [SerializeField]
    [ShowIf("@bulletNumber>1")]
    private float delayTime;
    public override bool StartAttack(Vector2 value,float angle, bool isflip, Actor actor)
    {
        base.StartAttack(value, angle, isflip, actor);
        StartCoroutine(FireBullets(value, angle, actor, isflip));
        return true;
    }
    private IEnumerator FireBullets(Vector2 value, float angle,Actor actor,bool isflip)
    {
        for (int i = 0; i < bulletNumber; i++)
        {
            Bullet temp = Instantiate(bullet, value, Quaternion.Euler(0, 0, angle)).GetComponent<Bullet>();
            temp.initialization(actor, isflip);
            yield return new WaitForSeconds(delayTime);
        }
    }
}
