using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Normal2 : AttackBase
{
    public GameObject bullet;
    public override bool StartAttack(Vector2 value, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(value, angle, isflip, actor);
        Bullet temp = Instantiate(bullet, value, Quaternion.Euler(0, 0, angle)).GetComponent<Bullet>();
        temp.initialization(actor, isflip);
        return true;
    }
}
