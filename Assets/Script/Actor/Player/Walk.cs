using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ProjectRun.Framwork.Utility;
[Serializable]
public class Walk : Movement
{
    private const float _deadzone = 0.3f;
    private const float _upSpeed = 32.0f;
    private Vector2 _axis;
    private Vector2 _inputAxis;
    private int MaxJumpnum = 2;
    private int JumpNum=0;
    [HideInInspector]
    public float JumpPower;
    private bool isDash;
    private bool isInAir;
    private bool isDoubleJump;
    private bool isSitting;
    private Vector2 _dashPower;
    private float DashAxis;
    private float dashDirection;
    private bool isobstructed;

    public bool IsSitting { get => isSitting; set => isSitting = value; }
    public bool IsDoubleJump { set => isDoubleJump = value; }
    public bool IsInAir { get => isInAir; set => isInAir = value; }
    public bool isObstructed { set => isobstructed = value; }
    public bool IsDash { get => isDash; set => isDash = value; }
    public float WalkPower { get => walkpower; set => walkpower = value; }
    public Vector2 InputAxis { get => _inputAxis; set => _inputAxis = value.normalized; }
    public float Deadzone { get => _deadzone; }

    private bool IsInDeadzone(float value) => Abs(value) <= _deadzone;

    /// <summary>
    /// 실행 - 가능한 FixedUpdate() 에서 루프해야합니다.
    /// </summary>
    /// <returns></returns>
    public override bool Do(float deltaTime)
    {
        if (isobstructed)
            return false;
        _axis.x = ScalaWalk(deltaTime, _inputAxis.x, _axis.x);
        if (isInAir && _axis.x == 0)
            _axis.x = Rigidbody.velocity.x;
        _axis.y = Rigidbody.velocity.y;

        if (_dashPower != Vector2.zero)
        {
            Rigidbody.velocity = _dashPower;
            return false;
        }
        else
        {
            if (!isSitting && !isInAir)
                Rigidbody.velocity = _axis;
            else if(!isSitting&& isInAir)
                Rigidbody.velocity = _axis;
            if (isInAir||_axis.x == 0||isSitting)
                return false;
            return true;
        }
    }
    public EJumpType jumpInWalk()
    {
        if (JumpNum == MaxJumpnum)
            return EJumpType.NONE;
        if (!isDoubleJump)
            JumpNum = 1;
        JumpNum++;
        //이코드 없으면 슈퍼점프 가능
        Rigidbody.velocity = new Vector2(Rigidbody.velocity.x, 0);

        Rigidbody.AddForce(Vector2.up* JumpPower);
        if (JumpNum == 1)
            return EJumpType.ONE;
        return EJumpType.SECOND;
    }
    public void JumpEnd()
    {
        JumpNum = 0;
    }
    public void GrabbingJumpCount()
    {
        JumpNum = 1;
    }
    public void DashStart(float power,float scaleX)
    {
        if (isDash)
            return;
        isDash = true;
        Vector2 dir = new Vector2(scaleX, 0);
        _dashPower = dir * power;
        DashAxis = Rigidbody.velocity.x;
        dashDirection = scaleX;
    }
    public void DashCancelCheck(Vector2 value)
    {
        if (!IsDash)
            return;
        if ((dashDirection == -1 && value.x > 0) || (dashDirection == 1 && value.x < 0))
            DashEnd();
    }
    public void DashEnd()
    {
        _dashPower = Vector2.zero;
        _axis = Vector2.zero;
        Rigidbody.velocity = new Vector2(DashAxis, 0);
        dashDirection =0;
    }
    private float ScalaWalk(float deltaTime, float input, float output)
    {
        // 이동 값이 없다면
        if (IsInDeadzone(input))
        {
            output = 0.0f;
        }
        //이동 값이 있다면
        else
        {
            // 이동방향
            var sign = GetSign(input);

            //만약 이동 방향이 아예다르면 0으로 초기화
            if (sign != GetSign(output))
            {
                output = 0.0f;
            }

            // 최대속력
            var zoneConst = _baseSpeed * Abs(input);
            // 이동한다.
            output += sign * _upSpeed * deltaTime;

            // 최고 속력을 넘으면
            if (Abs(output) > zoneConst)
            {
                // 최고 속력으로 마춰준다
                output = sign * zoneConst;
            }
        }
        return output;
    }
    public float Abs(float f) => f > 0 ? f : -f;
    public float GetSign(float number)
    {
        if (number > 0)
        {
            return 1.0f;
        }
        else if (number < 0)
        {
            return -1.0f;
        }
        else
        {
            return 0.0f;
        }
    }
}
