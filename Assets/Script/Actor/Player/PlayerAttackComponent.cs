using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectRun.Framwork.Utility;
using Sirenix.OdinInspector;
using Spine.Unity;

public class PlayerAttackComponent : AttackComponent
{
    private bool spineCombo;
    private bool isCombo;
    private bool isDash;
    private bool isClimb;
    private bool isAir;
    private bool isNormalUp;
    private bool isAttack;
    private string actionName;

    [SerializeField]
    [InlineButton("NewAttackList", "New")]
    protected CombatController attackList;
    protected bool isLast;

    [FoldoutGroup("Reload")]
    [SerializeField]
    private int reloadCount;
    [FoldoutGroup("Reload")]
    [SerializeField]
    private float reloadTime = 0.5f;
    private bool isRoard;
    private Coroutine reloardCoroutine;
    private int normalCount;

    private void NewAttackList()
    {
        if (attackList == null)
        {
            var newObject = new GameObject();

            newObject.name = "AttackList";
            newObject.transform.SetParent(transform);
            newObject.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            attackList = newObject.AddComponent<CombatController>();
            return;
        }
    }
    protected override void Start()
    {
        base.Start();
        attackList.SetComboData(ServiceLocatorManager.GetService<DataBaseManager>().LoadPlayerCombo());
        PlayerController Controller = ServiceLocatorManager.GetService<PlayerController>();
        Controller.OnAttackAction += (EAttackType value) =>
        {
            if (isDash||isClimb)
                return;
            Attack(value);
            isNormalUp = false;
        };
        PlayerMoveComponent moveComp = Owner.FindComponent<PlayerMoveComponent>();
        moveComp.OnMove += (bool value) =>
          {
              if (value)
                  actionName = "_Move";
              else
                  actionName = "_Idle";
          };
        moveComp.OnAir += (bool value) =>
          {
              isAir = value;
          };
        PlayerSpineComponent spineComp = Owner.FindComponent<PlayerSpineComponent>();
        spineComp.OnAttackStart += () =>
        {
            isAttack = true;
            isNormalUp = false;
        };
        spineComp.OnAttackComplite += () =>
        {
            spineCombo = false;
            isAttack = false;
            attackList.ResetAttackDataIfNotCombo();
        };
        spineComp.OnComboComplite += () =>
        {
            isCombo = false;
            spineCombo = false;
            isAttack = false;
            isNormalUp = false;
            attackList.ResetAttackData();
        };
        spineComp.OnAttackCombo += () =>
        {
            spineCombo = true;
        };
        //spineComp.OnComboStart += () =>
        //{
        //    isNormalUp = false;
        //    //canCombo = false;
        //};
        spineComp.OnShowLine += (Vector2 value, Vector2 dir,Actor owner) =>
        {
            attackList.ShowGuideLine(value, dir, owner);
        };
        spineComp.OnHideLine += () =>
        {
            attackList.HideGuideLine();
        };
        spineComp.OnAttackDo += (Vector2 value, float angle, bool flip,Actor actor) =>
        {
            attackList.StartAttack(value, angle, flip, actor);
        };
        spineComp.OnDashStarte += () =>
        {
            isDash = true;
        };
        spineComp.OnDashComplite += () =>
        {
            isDash = false;
        };
        spineComp.OnNormalUpComplite += () =>
        {
            isNormalUp = false;
        };
        spineComp.OnHangingStart += () =>
        {
            isClimb = true;
        };
        spineComp.OnClimbComplite += () =>
        {
            isClimb = false;
        };
        spineComp.OnJumpStart += () =>
        {
            isClimb = false;
        };
    }
    public override void Attack(EAttackType attackType)
    {
        base.Attack(attackType);
        if (isNormalUp)
            return;
        if(onlyNormalAttack())
        {
            attackList.ResetAttackData();
            attackList.SetAttack(EAttackType.X, false);
            isCombo = false;
            spineCombo = false;
        }
        else
        {        
            //콤보중에 콤보이벤트를 지나왔는지 
            if (isCombo && !spineCombo)
                return;
            //콤보가 끝났는지 확인
            if (!attackList.SetAttack(attackType, isCombo))
                return;
        }
        if (attackList.CanAttackCheck())
        {
            isNormalUp = true;
            string normalAttackName = attackList.GetNowAttackName();
            //다음공격이 콤보인지 확인
            if (!attackList.ComboCheck())
            {
                if (normalCount == reloadCount)
                    return;
                if (isAttack)
                    normalAttackName = normalAttackName + actionName + "_Down";
                else
                    normalAttackName = normalAttackName + actionName + "_Ready";
                OnAttackNameTrack01?.Invoke(normalAttackName, false);
                normalCount++;
                if (normalCount == reloadCount)
                {
                    reloardCoroutine = activeCoroutine(() => { normalCount = 0; isRoard = false; }, reloadTime);
                    isRoard = true;
                }
            }
            else
            {
                isCombo = true;
                spineCombo = false;
                normalAttackName= "Attack_" + normalAttackName + "_Combo";
                OnAttackNameTrack02?.Invoke(normalAttackName, false);
                if (isRoard)
                    StopCoroutine(reloardCoroutine);
                normalCount = 0;
            }
        }
    }
    private bool onlyNormalAttack()
    {
        return isAir || isDash;
    }
}
