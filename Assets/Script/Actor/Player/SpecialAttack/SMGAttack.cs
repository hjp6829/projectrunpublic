using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SMGAttack : AttackBase
{
    [SerializeField]
    private float Radius = 3;
    [SerializeField]
    private LayerMask EffectHitMask;
    [SerializeField]
    private BulletEffect effect;
    public override bool StartAttack(Vector2 value, float angle, bool isflip, Actor actor)
    {
        if (!base.StartAttack(value, angle, isflip, actor))
            return false;
        SpawnBulletEffect(isflip);
        return true;
    }
    private void SpawnBulletEffect(bool isFlip)
    {
        int startDir = isFlip ? 225 : -45;
        int endDir = isFlip ? 45 : 135;
        int step = isFlip ? -5 : 5;

        for (int i = startDir; isFlip ? (i > endDir) : (i < endDir); i += step)
        {
            CreateEffectAtHit(ShootRayFromPlayer(i, Radius));
        }
    }
    RaycastHit2D ShootRayFromPlayer(float angle, float distance)
    {
        Vector2 direction = Quaternion.Euler(0, 0, angle) * Vector2.right;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, direction, distance, EffectHitMask);
        return hit;
    }
    private void CreateEffectAtHit(RaycastHit2D hit)
    {
        if (!hit)
            return;
        BulletEffect Effect = Instantiate(effect);
        Vector2 Normal = hit.normal;
        Normal.x *= -1;
        float angle = Mathf.Atan2(Normal.x, Normal.y) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.Euler(0, 0, angle);
        Effect.transform.position = hit.point;
        Effect.transform.rotation = rotation;
        Effect.test();
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.DrawWireSphere(transform.position, Radius);
        Gizmos.color = Color.red;
    }
}
