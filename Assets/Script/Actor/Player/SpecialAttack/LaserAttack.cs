using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserAttack : AttackBase
{
    [SerializeField]
    private float Range = 3;
    [SerializeField]
    private LayerMask NormalHitMask;
    [SerializeField]
    private LayerMask HoldHitMask;
    [SerializeField]
    private int Damage;
    public override bool StartAttack(Vector2 value, float angle, bool isflip, Actor actor)
    {
        if (!base.StartAttack(value, angle, isflip, actor))
            return false;
        return true;
    }
}
