using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class ShotgunAttack : AttackBase
{
    [SerializeField]
    private float Range = 3;
    [SerializeField]
    private LayerMask EnemyHitMask;
    [SerializeField]
    private int Damage;
    public override bool StartAttack(Vector2 value,float angle, bool isflip,Actor actor)
    {
        if (!base.StartAttack(value, angle, isflip, actor))
            return false;
        Collider2D[] targets = Physics2D.OverlapCircleAll(value, Range, EnemyHitMask);
        for (int i=0;i<targets.Length;i++)
        {
            Vector2 targetDirection = ((Vector2)targets[i].transform.position - value).normalized;
            float Angle = Mathf.Atan2(targetDirection.x, targetDirection.y) * Mathf.Rad2Deg;
            if (Angle < 0)
                Angle += 360;
            if (!isflip && (Angle >= 0 && Angle <= 135))
                ExecuteEvents.Execute<IDamageable>(targets[i].gameObject, null, (a, b) => a.Hit(1,transform.position));
            else if(isflip && (Angle >= 225 && Angle <= 360))
                ExecuteEvents.Execute<IDamageable>(targets[i].gameObject, null, (a, b) => a.Hit(1, transform.position));
        }
        return true;
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.DrawWireSphere(transform.position, Range);
        Gizmos.color = Color.red;
    }
}
