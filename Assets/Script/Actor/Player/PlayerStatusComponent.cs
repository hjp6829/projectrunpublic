using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

public class PlayerStatusComponent : StatusCompoent
{
    private void ProcessDamage(int damage)
    {
        if (!isHit)
        {
            OnHit?.Invoke();
        }
        HP -= damage;
        OnHPChange?.Invoke(maxHP, HP, Shield);

        if (HP <= 0)
        {
            isDied = true;
            OnDie?.Invoke();
        }
    }
    private void ProcessShieldDamage(int damage)
    {
        if (Shield > 0)
        {
            Shield -= damage;
            if (Shield <= 0)
            {
                damage = -Shield;
                OnStatusName?.Invoke("Shield_Broken", false);
                return;
            }
            OnHPChange?.Invoke(maxHP, HP, Shield);
            if (!isHit)
            {
                OnHit?.Invoke();
            }
            return;
        }
        ProcessDamage(damage);
    }
    public override void Hit(int damage, Vector3 hitPos)
    {
        base.Hit(damage, hitPos);
        if (Shield > 0)
        {
            ProcessShieldDamage(damage);
        }
        else
        {
            ProcessDamage(damage);
        }
    }
}
