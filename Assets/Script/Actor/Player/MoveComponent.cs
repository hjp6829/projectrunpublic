using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
using ProjectRun.Framwork.Utility;
public class MoveComponent : ComponentBase
{
    #region Const
    protected const string MoveName = "Move";
    protected const string JumpName = "Jump";
    protected const string IdleName = "Idle";
    protected const string SittingIdleName = "Movement_Sit";
    protected const string DashName = "Dash";
    protected const string SlidingName = "Movement_Sliding";
    #endregion
    #region Action
    [HideInInspector] public Action<string, bool> OnMoveAction;
    [HideInInspector] public Action<bool> OnFlip;
    [HideInInspector] public Action<bool> OnMove;
    #endregion
    protected string NowSpineName;
    [SerializeField]
    protected Walk walk;
    [FoldoutGroup("TestSpeed")]
    [SerializeField]
    protected float Speed=5;
    protected bool isJump;
    protected bool isHit;
    protected bool isFlip;
    protected bool isCombo;

   public Walk Walk { get => walk; }
    private void Awake()
    {
        Owner.AddComponent(this);
    }
    protected virtual void Start()
    {
        SpineComponent spineComp = Owner.FindComponent<SpineComponent>();
        spineComp.OnSpineName += (string name) =>
          {
              NowSpineName = name;
          };

        spineComp.OnHitStart += () =>
        {
            isHit = true;
        };
        spineComp.OnHitComplite += () =>
        {
            isHit = false;
        };
        walk = new Walk();
        walk.Setter(RB, Speed);
    }
    protected virtual bool FixedUpdate()
    {
        if (isHit || isCombo)
        {
            InputAxis(Vector2.zero);
            return false;
        }
        Move();
        return true;
    }
    public virtual bool InputAxis(Vector2 value)
    {
        walk.InputAxis = value;
        if (value.x == 0)
            return false;
        return true;
    }
    public void SetFlip(float x)
    {
        if (x < 0)
        {
            isFlip = true;
            return;
        }
        isFlip = false;
    }
    protected virtual bool Move()
    {
        if (walk.Do(Time.deltaTime))
        {
            OnFlip?.Invoke(isFlip);
            OnMove?.Invoke(true);
            OnMoveAction?.Invoke(MoveName, true);
            return true;
        }
        return false;
    }
}
