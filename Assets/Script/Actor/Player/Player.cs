using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PlayerMoveComponent))]
[RequireComponent(typeof(PlayerSpineComponent))]
[RequireComponent(typeof(PlayerAttackComponent))]
public class Player : Actor
{

    private const string _bodyName = "LowerBody";
    private GameObject lowerbody;
    private bool isCrouchJumping;
    public GameObject GetLowerBody()
    {
        if (lowerbody != null)
            return lowerbody;
        try
        {
            lowerbody = transform.Find(_bodyName).gameObject;
        }
        catch (NullReferenceException)
        {
            if (lowerbody == null)
            {
                lowerbody = NewChild(_bodyName);
            }
        }
        return lowerbody;
    }
    private void Awake()
    {
        ServiceLocatorManager.GetService<GameManager>().Player = this;
    }
    private void Start()
    {
        
    }

}
