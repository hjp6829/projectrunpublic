using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Movement 
{
    protected float _baseSpeed;
    private Rigidbody2D _rigidbody;
    protected float walkpower;

    public Rigidbody2D Rigidbody { get => _rigidbody; }
    public float BaseSpeed { get => _baseSpeed; set => _baseSpeed = value; }

    public virtual void Setter(Rigidbody2D rigidbody, float baseSpeed)
    {
        _rigidbody = rigidbody;
        _baseSpeed = baseSpeed;
        walkpower = 1;
    }

    /// <summary>
    /// Do - 움직임을 실행합니다.
    /// </summary>
    public abstract bool Do(float deltaTime);
}
