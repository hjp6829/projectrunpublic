using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
public class Entity : SerializedMonoBehaviour
{
    [SerializeField] private int _nInDex = -1;
    public int Index => _nInDex;

    private Dictionary<Type, SerializedMonoBehaviour> _dicPairComponent = new Dictionary<Type, SerializedMonoBehaviour>();
    public bool RegisterComponent<T>(T RegisterComponent) where T : SerializedMonoBehaviour, new()
    {
        var varComponentType = typeof(T);
        if (_dicPairComponent.ContainsKey(varComponentType) == true)
        {
            return false;
        }

        else
        {
            AddComponent<T>(RegisterComponent);
            return true;
        }
    }
    public bool ContainComponent<T>() where T : SerializedMonoBehaviour, new()
    {
        var varComponentType = typeof(T);
        if (_dicPairComponent.ContainsKey(varComponentType))
            return true;
        return false;
    }
    public T FindComponent<T>() where T : SerializedMonoBehaviour, new()
    {
        var varComponentType = typeof(T);
        if (_dicPairComponent.ContainsKey(varComponentType) == true)
        {
            return _dicPairComponent[varComponentType] as T;
        }

        else
        {
            if (TryGetComponent<T>(out T getComponent))
            {
                _dicPairComponent.Add(typeof(T), getComponent);
                return getComponent;
            }

            else
            {
                throw new MissingComponentException($"{gameObject.name} ���� {varComponentType}�� ã�� �� �����ϴ�");
            }
        }
    }
    public T AddComponent<T>(T addComponentType) where T : SerializedMonoBehaviour, new()
    {
        var varComponentType = typeof(T);

        try
        {
            return FindComponent<T>();
        }
        catch (MissingComponentException)
        {
            var varAddComponent = gameObject.AddComponent<T>();
            _dicPairComponent.Add(varComponentType, varAddComponent);
            return varAddComponent;
        }
    }
}
