using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using ProjectRun.Framwork.Utility;
using System;
using static UnityEngine.UI.CanvasScaler;

public class PlayerMoveComponent : MoveComponent
{
    [HideInInspector] public Action<bool> OnAir;
    [HideInInspector] public Action<bool> OnSit;
    #region Dash
    [FoldoutGroup("TestDash")]
    [SerializeField]
    private float DashPower = 15.0f;
    [FoldoutGroup("TestDash")]
    [SerializeField]
    private float DashTime = 0.3f;
    [FoldoutGroup("TestDash")]
    [SerializeField]
    private float DashCoolTime = 1f;
    #endregion
    [SerializeField]
    private LayerMask WallDetection;
    [FoldoutGroup("TEST")]
    [Range(0,0.25f)]
    [SerializeField]
    private float DownRayLeft=0.1f;
    [FoldoutGroup("TEST")]
    [Range(0, 0.25f)]
    [SerializeField]
    private float DownRayRight=0.1f;
    [FoldoutGroup("TEST")]
    [Range(0, 1f)]
    [SerializeField]
    private float DownRayJumpCheck = 0.5f;
    private float wallDetectionDistance = 0.3f;
    [SerializeField]
    private float groundDetectionDistance = 0.6f;
    [FoldoutGroup("TestSpeed")]
    [SerializeField]
    private float dashBaseSpeed;
    [FoldoutGroup("TestSpeed")]
    [SerializeField]
    private float tapJumpPower = 200;
    [FoldoutGroup("TestSpeed")]
    [SerializeField]
    private float holdJumpPower = 300;
    [FoldoutGroup("TestSpeed")]
    [SerializeField]
    private float dashJumpPower = 300;
    private bool isdashRoutine;
    private bool isTilePass;
    private Coroutine dashRoutine;

    #region Climb
    private Vector2 startPosition;            // 시작 위치
    private Vector2 targetPosition;           // 목표 위치
    private Vector2 velocity;                 // 속도
    private float gravity;       // 중력 가속도
    private float elapsedTime = 0.0f;         // 경과 시간
    [SerializeField]
    private float climbTime = 2.0f;         // 전체 이동 시간
    private bool isClimb;
    #endregion 
    [FoldoutGroup("LEDGEGRAB")]
    [SerializeField]
    private float redYOffset, GreenYOffset;
    private float startingGravity;
    private bool RedBox, GreenBox;
    private bool isGrabbing;
    private EMoveDir moveDir;

    protected override void Start()
    {
        Owner.OnCollderExit += (LayerMask layer) =>
        {
            switch(layer)
            {
                case 10:
                    activeCoroutine(() => isTilePass = false, 0.1f);
                    GetCollider.isTrigger = false;
                    break;
            }
        };
        OnSit += (bool value) =>
        {
            walk.IsSitting = value;
            CapsuleCollider2D playerCollider = (CapsuleCollider2D)GetCollider;
            if (value)
            {
                playerCollider.size = new Vector2(playerCollider.size.x, 0.4f);
                return;
            }
            playerCollider.size = new Vector2(playerCollider.size.x, 0.8f); ;
        };
        PlayerController Controller = ServiceLocatorManager.GetService<PlayerController>();
        Controller.OnMoveAxis += (Vector2 value) =>
        {
            if (value.y<0 && !walk.IsInAir)
                OnSit(true);
            else if (walk.IsSitting&& value.y>=0)
                OnSit(false); 
            InputAxis(value);
            walk.DashCancelCheck(value);
        };
        Controller.OnInputDir += (EMoveDir dir) =>
        {
            moveDir = dir;
        };
        Controller.OnInputGrbbing += () =>
        {
            GrabbingAction();
        };
        Controller.OnJumpAction += (bool value) =>
        {
            if (walk.IsDash&&walk.IsInAir||isCombo)
                return;
            if (walk.IsSitting && isTilePass == false)
            {
                Vector2 tilePassDownLeft = new Vector2(RB.position.x - ((GetCollider.bounds.size.x * 0.5f) - DownRayLeft), RB.position.y - GetCollider.bounds.size.y * 0.5f);
                Vector2 tilePassDownRight = new Vector2(RB.position.x + ((GetCollider.bounds.size.x * 0.5f) - DownRayRight), RB.position.y - GetCollider.bounds.size.y * 0.5f);
                RaycastHit2D hitTilePassDownLeft = GetRaycastHit(tilePassDownLeft, Vector2.down, 0.1f, 1 << 10, Color.red);
                RaycastHit2D hitTilePassDownRight = GetRaycastHit(tilePassDownRight, Vector2.down, 0.1f, 1 << 10, Color.red);
                if (hitTilePassDownLeft || hitTilePassDownRight)
                {
                    GetCollider.isTrigger = true;
                    isTilePass = true;
                    return;
                }
                return;
            }
                if (isdashRoutine)
            {
                StopCoroutine(dashRoutine);
                isdashRoutine = false;
                walk.IsDash = false;
                walk.DashEnd();
                walk.BaseSpeed = dashBaseSpeed;
                walk.JumpPower = dashJumpPower;
            }
            else
            {
                if(value)
                    walk.JumpPower = holdJumpPower;
                else
                    walk.JumpPower = tapJumpPower;
                walk.BaseSpeed = Speed; 
            }
            JumpInPlayerMove();
        };
        OnAir += (bool value) =>
        {
            if (isTilePass)
            {
                walk.IsInAir = true;
                return;
            }
            walk.IsInAir = value;
        };
        Controller.OnDashAction += () =>
        {
            if (walk.IsDash|| isClimb || isGrabbing||isCombo|| isTilePass)
                return;
            walk.DashStart(DashPower,Owner.GetBody().transform.localScale.x);
            if(walk.IsSitting)
                OnMoveAction?.Invoke(SlidingName, false);
            else
                OnMoveAction?.Invoke(DashName, false);
            isdashRoutine = true;
            dashRoutine = activeCoroutine(() =>
            {
                activeCoroutine(() => walk.IsDash = false, DashCoolTime);
                isdashRoutine = false;
                walk.DashEnd();
            }, DashTime);
        };
        startingGravity = RB.gravityScale;
        PlayerSpineComponent spineComp = Owner.FindComponent<PlayerSpineComponent>();
        spineComp.OnClimbComplite += () =>
        {
            PlayerController.isInput = true;
            isClimb = false;
            RB.gravityScale = startingGravity;
            isGrabbing = false;
        };
        spineComp.OnComboComplite += () =>
        {
            isCombo = false;
        };
        spineComp.OnComboStart += () =>
        {
            //RB.velocity = Vector2.zero;
            isCombo = true;
        };
        base.Start();
    }
    protected override bool FixedUpdate()
    {
        if (isClimb)
            return false;
        if (!base.FixedUpdate())
            return false;
        Vector2 direction = !isFlip ? Vector2.right : Vector2.left;
        float Jumpchecker= !isFlip ? 1 : -1;
        Vector2 CrouchJumpPos = new Vector2(RB.position.x, RB.position.y - ((GetCollider.bounds.size.y * 0.5f) + 0.1f));
        //Vector2 bottomColliderPos = new Vector2(RB.position.x, RB.position.y - ((GetCollider.bounds.size.y * 0.5f)- 0.01f));
        Vector2 upColliderPos = new Vector2(RB.position.x, RB.position.y + ((GetCollider.bounds.size.y * 0.5f) - 0.1f));
        Vector2 GroundLeft = new Vector2(RB.position.x - ((GetCollider.bounds.size.x * 0.5f)- DownRayLeft), RB.position.y);
        Vector2 GroundRight = new Vector2(RB.position.x + ((GetCollider.bounds.size.x * 0.5f)- DownRayRight), RB.position.y);
        Vector2 GroundJumpChecker = new Vector2(RB.position.x + (((GetCollider.bounds.size.x * 0.5f) - DownRayRight- DownRayJumpCheck) * Jumpchecker), RB.position.y);
        Vector2 tilePassLeft = new Vector2(RB.position.x - ((GetCollider.bounds.size.x * 0.5f) - DownRayLeft), RB.position.y + GetCollider.bounds.size.y * 0.5f);
        Vector2 tilePassRight = new Vector2(RB.position.x + ((GetCollider.bounds.size.x * 0.5f) - DownRayRight), RB.position.y + GetCollider.bounds.size.y * 0.5f);

        RaycastHit2D hitGroundRight = GetRaycastHit(GroundRight, Vector2.down, groundDetectionDistance, WallDetection, Color.black);
        RaycastHit2D hitGroundLeft = GetRaycastHit(GroundLeft, Vector2.down, groundDetectionDistance, WallDetection, Color.black);
        RaycastHit2D hitGroundJumpChecker = GetRaycastHit(GroundJumpChecker, Vector2.down, groundDetectionDistance, WallDetection, Color.white);
        //RaycastHit2D hitSideDown = GetRaycastHit(bottomColliderPos, direction, wallDetectionDistance, WallDetection, Color.yellow);
        RaycastHit2D hitSideCenter = GetRaycastHit(RB.position, direction, wallDetectionDistance, WallDetection, Color.yellow);
        RaycastHit2D hitSideUp = GetRaycastHit(upColliderPos, direction, wallDetectionDistance, WallDetection, Color.yellow);
        RaycastHit2D hitTilePassLeft= GetRaycastHit(tilePassLeft, Vector2.up, 0.03f, 1<<10, Color.red);
        RaycastHit2D hitTilePassRight = GetRaycastHit(tilePassRight, Vector2.up, 0.03f, 1 << 10, Color.red);
        //RaycastHit2D hitCrouchJump = GetRaycastHit(CrouchJumpPos, direction, wallDetectionDistance, WallDetection, Color.grey);
        #region Grabbing
        Vector2 RedRayPos = new Vector2(RB.position.x, RB.position.y + ((GetCollider.bounds.size.y * 0.5f) - redYOffset));
        Vector2 GreenRayPos = new Vector2(RB.position.x, RB.position.y + ((GetCollider.bounds.size.y * 0.5f) - GreenYOffset));
        GreenBox = GetRaycastHit(GreenRayPos, direction, wallDetectionDistance, WallDetection, Color.green);
        RedBox = GetRaycastHit(RedRayPos, direction, wallDetectionDistance, WallDetection,Color.red);

        walk.IsDoubleJump = hitGroundJumpChecker;
        OnAir?.Invoke(!hitGroundRight && !hitGroundLeft);
        EMoveDir GrbbingDir = !isFlip ? EMoveDir.RIGHT : EMoveDir.LEFT;
        if (!isTilePass && (hitTilePassLeft || hitTilePassRight))
        {
            isTilePass = true;
            GetCollider.isTrigger = true;
        }
        if (!GreenBox && RedBox && walk.IsInAir && !isGrabbing&& moveDir== GrbbingDir&& !isTilePass)
        {
            if (RB.velocity.y < 0)
            { 
                isGrabbing = true;
                OnMoveAction?.Invoke("Hanging", false);
            }
        }
        else if (!GreenBox && !RedBox && walk.IsInAir && isGrabbing)
        {
            RB.gravityScale = startingGravity;
            isGrabbing = false;
        }
        if (isGrabbing)
        {
            RB.velocity = Vector2.zero;
            RB.gravityScale = 0;
            walk.GrabbingJumpCount();
            return true;
        }
        #endregion
        if (hitGroundRight|| hitGroundLeft)
        {
            if (isJump)
            {
                walk.JumpEnd();
                walk.BaseSpeed = Speed;
                walk.JumpPower = 300;
                isJump = false;
            }
        }
        else if (walk.IsInAir)
            OnMoveAction?.Invoke(JumpName, false);
        walk.isObstructed =  hitSideCenter|| hitSideUp;
        return true;
    }
    private void GrabbingAction()
    {
        if (!isGrabbing || moveDir != EMoveDir.UP)
            return;
        PlayerController.isInput = false;
        isClimb = true;
        StartCoroutine(ClimbAction());
        OnMoveAction?.Invoke("Climb", false);
    }
    private RaycastHit2D GetRaycastHit(Vector2 startPoint, Vector2 direction, float distance, LayerMask layerMask,Color color)
    {
        RaycastHit2D hit = Physics2D.Raycast(startPoint, direction, distance, layerMask);
        Debug.DrawRay(startPoint, direction * distance, color);
        return hit;
    }
    private void JumpInPlayerMove()
    {
        EJumpType jumpType = walk.jumpInWalk();
        if (jumpType == EJumpType.NONE)
            return;
        switch(jumpType)
        {
            case EJumpType.ONE:
                OnMoveAction?.Invoke(JumpName, false);
                break;
            case EJumpType.SECOND:
                OnMoveAction?.Invoke(JumpName, false);
                break;
        }
        activeCoroutine(() => isJump = true, 0.2f);
    }
    protected override bool Move()
    {
        if (base.Move())
            return true;
        if (walk.IsInAir && !isdashRoutine)
        {
            OnFlip?.Invoke(isFlip);
            return false;
        }
        if (walk.IsInAir || isdashRoutine)
            return false;
        OnMove?.Invoke(false);
        string idleActionName;
        if (walk.IsSitting)
            idleActionName = SittingIdleName;
        else
            idleActionName = IdleName;
        if(walk.IsSitting)
            OnFlip?.Invoke(isFlip);
        if (NowSpineName != idleActionName)
        {
            OnMoveAction?.Invoke(idleActionName, true);
            return true;
        }
        return false;
    }
    public override bool InputAxis(Vector2 value)
    {
        if (!base.InputAxis(value))
            return false;
        SetFlip(value.x);
        return true;
    }
    IEnumerator ClimbAction()
    {
        float newXOffest = isFlip ? -0.513585f : 0.513585f;
        Vector2 newGrabbingPos = new Vector2(transform.position.x + newXOffest, transform.position.y + 0.97f);
        startPosition = Owner.transform.position;           // 시작위치를 현재 오브젝트의 위치로 설정
        targetPosition = newGrabbingPos;             // 목표위치를 목표물의 위치로 설정
        gravity = startingGravity;                                             // 속도 계산
        velocity = (targetPosition - startPosition) / climbTime;
        velocity.y = (targetPosition.y - startPosition.y + 0.5f * gravity * Mathf.Pow(climbTime, 2)) / climbTime;
        elapsedTime =0;
        while (true)
        {
            elapsedTime += Time.deltaTime;

            // 경과 시간이 전체 이동시간 이내면 위치를 업데이트, 아니라면 목표 위치로 위치 설정
            if (elapsedTime <= climbTime)
            {
                float t = elapsedTime;
                Owner.transform.position = new Vector2(
                    startPosition.x + velocity.x * t,
                    startPosition.y + velocity.y * t - 0.5f * gravity * Mathf.Pow(t, 2)
                );
            }
            else
            {
                Owner.transform.position = newGrabbingPos;
                break;
            }
            yield return null;
        }
    }
}
