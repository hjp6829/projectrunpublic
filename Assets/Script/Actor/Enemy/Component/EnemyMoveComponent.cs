using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMoveComponent : MoveComponent
{
    private bool isAttack;
    protected override void Start()
    {
        base.Start();
        AIController AI= Owner.FindComponent<AIController>();
        AI.OnMove += (Vector2 Axis) =>
        {
            InputAxis(Axis);
        };
        AI.OnFlip += (float X) =>
        {
            SetFlip(X);
        };
        SpineComponent spineComp = Owner.FindComponent<SpineComponent>();
        spineComp.OnAttackStart += () =>
        {
            isAttack = true;
        };
        spineComp.OnAttackComplite += () =>
        {
            isAttack = false;
        };
    }
    protected override bool Move()
    {
        if (base.Move())
            return true;
        if (isAttack)
            return false;
        if (NowSpineName != IdleName)
        {
            OnMoveAction?.Invoke(IdleName, true);
            return true;
        }
        return false;

    }
}
