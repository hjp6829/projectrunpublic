using ProjectRun.Framwork.Utility;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 2023.07.15
/// ���ʹ� �÷��̾�ó�� �Ϲ�,����Ȱ����� �־������ 0�����ݸ� �����
/// �׷��� ������ ��Ÿ���� ��������ؼ� ���θ���
/// </summary>
public class EnemyAttackComponent : AttackComponent
{
    [SerializeField]
    [FoldoutGroup("affix")]
    private string prefix;
    [SerializeField]
    [FoldoutGroup("affix")]
    private string suffix;
    private int currentPhase;
    [SerializeField]
    [InlineButton("NewAttackList", "New")]
    protected List<CombatController> attackList=new List<CombatController>();

    private void NewAttackList()
    {
        if (attackList.Count == 0)
        {
            var newObject = new GameObject();
            newObject.name = "AttackListTemp";
            newObject.transform.SetParent(transform);
            newObject.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            attackList.Add(newObject.AddComponent<CombatController>());
            return;
        }
    }
    protected override void Start()
    {
        base.Start();
        if (Owner.ContainComponent<AIController>())
        {
            AIController AI = Owner.FindComponent<AIController>();
            AI.OnAttack += (EAttackType type) =>
            {
                Attack(type);
            };
        }
        SpineComponent spineComp = Owner.FindComponent<SpineComponent>();
        spineComp.OnAttackDo += (Vector2 value, float angle, bool flip,Actor actor) =>
        {
            foreach(var attack in attackList)
                attack.StartAttack(value, angle, flip, actor);
        };
    }
    public override void Attack(EAttackType attackType)
    {
        base.Attack(attackType);
        attackList[currentPhase].ResetAttackData();
        attackList[currentPhase].SetAttack(EAttackType.X, false);
        if (!attackList[currentPhase].CanAttackCheck())
            return;
        OnAttackNameTrack02?.Invoke(prefix + attackList[currentPhase].GetNowAttackName() + suffix, false);
        idleTime = attackList[currentPhase].GetNowAttackIdleTime();
        OnSetIdleTime?.Invoke(idleTime);
    }
}
