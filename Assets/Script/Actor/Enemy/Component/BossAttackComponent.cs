using ProjectRun.Framwork.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;

public class BossAttackComponent : AttackComponent
{
    private int currentPhase;
    [SerializeField]
    [InlineButton("NewAttackList", "New")]
    protected List<CombatController> attackList = new List<CombatController>();

    private void NewAttackList()
    {
            var newObject = new GameObject();
            newObject.name = "AttackListTemp";
            newObject.transform.SetParent(transform);
            newObject.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);
            attackList.Add(newObject.AddComponent<CombatController>());
    }

    protected override void Start()
    {
        base.Start();
        if (Owner.ContainComponent<AIController>())
        {
            AIController AI = Owner.FindComponent<AIController>();
            AI.OnAttack += (EAttackType type) =>
            {
                Attack(type);
            };
            AI.OnPhaseSet += (int value) =>
            {
                currentPhase = value;
            };
        }
        BossSpineComponent spineComp = Owner.FindComponent<BossSpineComponent>();
        spineComp.OnAttackDo += (Vector2 value, float angle, bool flip, Actor actor) =>
        {
            foreach(var attack in attackList)
                attack.StartAttack(value, angle, flip, actor);
        };
        spineComp.OnShowLine += (Vector2 start, Vector2 dir,Actor owner) =>
        {
            foreach (var attack in attackList)
                attack.ShowGuideLine(start, dir, owner);
        };
        spineComp.OnHideLine += () =>
        {
            foreach (var attack in attackList)
                attack.HideGuideLine();
        };
        spineComp.OnAttackEndStart += (Actor actor) =>
        {
            foreach (var attack in attackList)
                attack.EndAttackStart(actor);
        };
    }
    public override void Attack(EAttackType attackType)
    {
        base.Attack(attackType);
        attackList[currentPhase].ResetAttackData();
        attackList[currentPhase].SetAttack(attackType, false);
        if (!attackList[currentPhase].CanAttackCheck())
            return;
        OnAttackNameTrack02?.Invoke("Attack_"+attackList[currentPhase].GetNowAttackName()+"_Start", false);
        idleTime = attackList[currentPhase].GetNowAttackIdleTime();
        OnSetIdleTime?.Invoke(idleTime);
    }
}
