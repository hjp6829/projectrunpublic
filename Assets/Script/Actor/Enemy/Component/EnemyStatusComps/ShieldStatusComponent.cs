using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldStatusComponent : StatusCompoent
{
    public override void Hit(int damage, Vector3 hitPos)
    {
        base.Hit(damage, hitPos);
        float cross = Vector3.Cross(transform.position - hitPos, Vector2.up).z;
        float scaleX = Owner.GetBody().transform.localScale.x;

        if ((scaleX == 1 && cross > 0) || (scaleX == -1 && cross < 0))
        {
            ProcessDamage(damage);
            Debug.Log("등뒤공격");
        }
        else
        {
            Debug.Log("dkvdptj공격");
            ProcessShieldDamage(damage);
        }

        Debug.Log(cross);
    }
    private void ProcessDamage(int damage)
    {
        if (!isHit)
        {
            OnHit?.Invoke();
        }
        HP -= damage;
        OnHPChange?.Invoke(maxHP, HP, Shield);

        if (HP <= 0)
        {
            isDied = true;
            OnDie?.Invoke();
        }
    }
    private void ProcessShieldDamage(int damage)
    {
        if (Shield > 0)
        {
            Shield -= damage;
            if (Shield <= 0)
            {
                damage = -Shield;
                OnStatusName?.Invoke("Shield_Broken", false);
                return;
            }
            OnHPChange?.Invoke(maxHP, HP, Shield);
            if (!isHit)
            {
                OnHit?.Invoke();
            }
            return;
        }
        ProcessDamage(damage);
    }
    public override void Hit(int damage)
    {
        base.Hit(damage);
        if (Shield > 0)
        {
            ProcessShieldDamage(damage);
        }
        else
        {
            ProcessDamage(damage);
        }
    }
}
