using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ProjectRun.Framwork.Utility;
using UnityEditor.U2D.Sprites;

public class AIController : ComponentBase
{
    #region action
    [HideInInspector]
    public Action<Vector2> OnMove;
    [HideInInspector]
    public Action<float> OnFlip;
    [HideInInspector]
    public Action<EAttackType> OnAttack;
    [HideInInspector] public Action<int> OnPhaseSet;
    #endregion
    private Player player;
    [SerializeField]
    private float AttackRange=2;
    [SerializeField]
    private float RecognitionRange=5;
    private Vector2 Axis;
    private bool isAttackSpine;
    [SerializeField]
    private float attackPauseTime = 3;
    [SerializeField]
    protected BehaviourTree tree;
    private bool isFlip;

    public BehaviourTree Tree { get => tree; }
    public Player Player { get => player; }
    private void Awake()
    {
        Owner.AddComponent(this);
    }
    protected virtual void Start()
    {
        player = ServiceLocatorManager.GetService<GameManager>().Player;
        tree = tree.Clone();
        tree.Bind(Owner, this);
        OnFlip += (float value) =>
        {
            SetFlip(value);
        };
        (Owner as Enemy).OnPlayerInArea += (bool value) =>
     {
         tree.blackboard.IsInArea = value;
     };
        SpineComponent spineComp = Owner.FindComponent<SpineComponent>();
        spineComp.OnAttackStart += () =>
        {
            Axis = (player.transform.position - transform.position).normalized;
            if (Axis.x < 0)
                spineComp.SetFlip(true);
            else if (Axis.x > 0)
                spineComp.SetFlip(false);

            isAttackSpine = true;
        };
        spineComp.OnAttackComplite += () =>
        {
            if (attackPauseTime < 0)
            {
                tree.blackboard.IsAttack = false;
                isAttackSpine = false;
                return;
            }
            activeCoroutine(() =>
            {
                tree.blackboard.IsAttack = false;
                isAttackSpine = false;
            }, attackPauseTime);
        };
        spineComp.OnHitStart += () => tree.blackboard.IsHit = true;
        spineComp.OnHitComplite += () => tree.blackboard.IsHit = false;
        StatusCompoent statusComp = Owner.FindComponent<StatusCompoent>();
        statusComp.OnDie += () =>
        {
            this.enabled = false;
        };
    }
    protected virtual void Update()
    {
        tree.Update();
    }
    public void SetParabolaData(float time, Transform targetPos)
    {
        tree.blackboard.IsParabola = true;
        tree.blackboard.timeToTarget = time;
        tree.blackboard.targetPosition = targetPos;
    }
    public void OneWayInput(EOneWayDir dir)
    {
        Vector2 Axis=Vector2.zero;
        switch (dir)
        {
            case EOneWayDir.LEFT:
                Axis = new Vector2(-1, 0);
                break;
            case EOneWayDir.RIGHT:
                Axis = new Vector2(1, 0);
                break;
        }
        OnFlip?.Invoke(Axis.x);
        OnMove?.Invoke(Axis);
    }
    public void ChaseTargetPos(Vector2 targetPos)
    {
        Vector2 Pos=new Vector2(targetPos.x, transform.position.y);
        Axis = (Pos - (Vector2)transform.position).normalized;
        if (!isAttackSpine)
            OnFlip?.Invoke(Axis.x); 
        if (!tree.blackboard.IsAttack)
        {
            OnMove?.Invoke(Axis); 
        }
    }
    public void StopEnemy()
    {
        Axis = Vector2.zero;
        OnMove?.Invoke(Axis);
    }
    public void AttackPlayer(EAttackType type)
    {
        if (tree.blackboard.IsAttack)
            return;
        tree.blackboard.IsAttack = true;
        OnAttack?.Invoke(type);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawWireSphere(transform.position, RecognitionRange);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, AttackRange);
    }
    private bool CheckFall()
    {
        int posX = Mathf.RoundToInt(transform.position.x);
        int posY = Mathf.RoundToInt(transform.position.y);
        Vector2Int nowPos = new Vector2Int(posX, posY-1);
        //if ((RB.velocity.x < 0 || RB.velocity.x > 0) && !tilemanager.MapTiles.ContainsKey(nowPos))
        //{
        //    return true;
        //}
        return false;
    }
    public void OscillateAroundTarget(float value)
    {
        Transform target = tree.blackboard.Target;
        float ownerY = Owner.transform.position.y;
        Vector2 RightPos = new Vector2(target.position.x + 10, ownerY);
        Vector2 LeftPos = new Vector2(target.position.x - 10, ownerY);
        Vector2 TargetPos = transform.position;
        if (isFlip) TargetPos = LeftPos;
        else TargetPos = RightPos;

        if (Vector2.Distance(TargetPos, Owner.transform.position) < 1)
        {
            if(isFlip) TargetPos = RightPos;
            else TargetPos =LeftPos;
        }
        ChaseTargetPos(TargetPos);
    }
    public void SetFlip(float x)
    {
        if (x < 0)
        {
            isFlip = true;
            return;
        }
        isFlip = false;
    }
}
