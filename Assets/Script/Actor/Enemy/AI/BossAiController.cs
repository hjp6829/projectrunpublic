using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(BossSpineComponent))]
[RequireComponent(typeof(BossAttackComponent))]
public class BossAiController : AIController
{
    [SerializeField]
    private Transform testBossSpawnPos;
    [SerializeField]
    private bool testMonsterPause;

    protected override void Start()
    {
        base.Start();
        tree.blackboard.Target = testBossSpawnPos;
    }
    protected override void Update()
    {
        tree.blackboard.IsPause = testMonsterPause;
        base.Update();
    }
}
