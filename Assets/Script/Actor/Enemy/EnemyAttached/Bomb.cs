using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Bomb : Actor
{
    [SerializeField]private float speed;
    [SerializeField] private int damage;

    public int Damage { set => damage = value; }
    private void FixedUpdate()
    {
        Rigidbody.velocity=Vector2.down* speed*10*Time.deltaTime;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch(collision.gameObject.layer)
        {
            case 4:
                Destroy(this.gameObject);
                break;
            case 7:
                ExecuteEvents.Execute<IDamageable>(Collider2D.gameObject, null, (a, b) => a.Hit(damage,transform.position));
                Destroy(this.gameObject);
                break;
        }
    }
}
