using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using ProjectRun.Framwork.Utility;

public class DroneAttached : Actor
{
    [HideInInspector] public Action OnNextSpwan;
    [HideInInspector] public Action OnSetUp;
    [HideInInspector] public bool isSetUP;
    [HideInInspector] public bool isLine;
    public LinePrediction line;
    private LinePrediction lineRenderer;
    private Vector2 spawnPos;
    private Vector2 TargetPos;
    private float Distance;
    private float Speed;

    public void ShowLine()
    {
        lineRenderer = Instantiate(line);
        lineRenderer.Show(transform.position, Vector2.left, 50);
    }
    public void HideLine()
    {
        if (lineRenderer != null)
            lineRenderer.Hide(0);
    }
    public void AttackDrone(EAttackType attackType)
    {
        EnemyAttackComponent attackComp = FindComponent<EnemyAttackComponent>();
        attackComp.Attack(attackType);
    }
    public void Spwn(Vector2 targetPos,float distance,float moveSpeed, DroneAttached nextDrone)
    {
        spawnPos = transform.position;
        this.TargetPos = targetPos;
        this.Distance = distance;
        this.Speed = moveSpeed;
        OnNextSpwan = () =>
        {
            if(nextDrone!=null)
                nextDrone.MoveStart();
        };
    } 
    public void MoveStart()
    {
        StartCoroutine(MovePos(TargetPos, Distance));
    }
    IEnumerator MovePos(Vector2 targetPos,float dis)
    {
        bool isNextSpawn=false;
        while(true)
        {
            if (Vector2.Distance(transform.position, targetPos) <= 0.5f)
            {
                isSetUP = true;
                OnSetUp?.Invoke();
                break;
            }
            if(!isNextSpawn && Vector2.Distance(transform.position, spawnPos) >= dis)
            {
                isNextSpawn = true;
                OnNextSpwan?.Invoke();
            }
            Vector2 newPos = new Vector2(transform.position.x, transform.position.y - Speed * Time.deltaTime);
            transform.position = newPos;
            yield return null;
        }
    }
}
