using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem.HID;

public class BombingSpawner : MonoBehaviour
{
   [SerializeField] private LinePrediction bombingLine;
    [SerializeField] private Bomb bomb;
    private LinePrediction line;
    private Dictionary<int, Vector2> attackPos = new Dictionary<int, Vector2>();
    public void MakeGuideLine(int idx,float y)
    {
        Vector2 linePos = new Vector2(transform.position.x,transform.position.y + y);
        attackPos.Add(idx, linePos);
        RaycastHit2D hit = Physics2D.Raycast(linePos, Vector2.down,100,1<<4);
        line = Instantiate(bombingLine, linePos, bombingLine.transform.rotation);
        line.Show(linePos, Vector2.down, Vector2.Distance(linePos, hit.point));
    }
    public void MakeBombStart(int idx,int damage)
    {
        HideLine();
        if (!attackPos.ContainsKey(idx))
            return;
        Bomb bombTemp = Instantiate(bomb, attackPos[idx], bomb.transform.rotation);
        bombTemp.Damage = damage;
    }
    public void HideLine()
    {
        if (line != null)
            line.Hide(0);
    }
}
