using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.EventSystems;

public class Enemy : Actor
{
    [HideInInspector]
    public Action<bool> OnPlayerInArea;
    [SerializeField] private int contactDamage=1;
    private void Start()
    {
        StatusCompoent statusComp = FindComponent<StatusCompoent>();
        statusComp.OnDie += () =>
        {
            Destroy(gameObject);
            //GetBody().SetActive(false);
            //Collider2D.isTrigger = true;
        };
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 7:
                ExecuteEvents.Execute<IDamageable>(collision.gameObject, null, (a, b) => a.Hit(contactDamage,transform.position));
                break;
        }
    }
}
