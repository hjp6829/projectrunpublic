using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.UI.GridLayoutGroup;

public class DashAttack : AttackBase
{
    [SerializeField] private float dashSpeed;
    private Actor owner;
    public override bool StartAttack(Vector2 FirePos, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(FirePos, angle, isflip, actor);
        owner=actor;
        EnemyMoveComponent enemyMoveComp = actor.FindComponent<EnemyMoveComponent>();
        enemyMoveComp.Walk.DashStart(-dashSpeed, actor.GetBody().transform.localScale.x);
        return true;
    }
    private IEnumerator DashEndCheck(Vector2 targetPos)
    {
        while (true)
        {
            if (Vector2.Distance(owner.transform.position, targetPos) <= 0.2f)
            {
                EnemyMoveComponent enemyMoveComp = owner.FindComponent<EnemyMoveComponent>();
                enemyMoveComp.Walk.DashEnd();
                break;
            }
            yield return null;
        }
    }
}
