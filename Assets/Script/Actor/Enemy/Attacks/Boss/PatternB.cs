using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using ProjectRun.Framwork.Utility;
[Serializable]
public class patternBData
{
    public float attack1Angle;
    public float attack2Angle;
}
public class PatternB : AttackBase
{
    [SerializeField]
    private List<patternBData> attackData= new List<patternBData>();
    [SerializeField] private Transform firePosTest;
    [SerializeField] private GameObject bullet;
    [SerializeField] private int debugRayNumber;
    [SerializeField] private Actor owner;
    [SerializeField] private float height;
    [SerializeField] private LinePrediction linePrediction;
    private int heightLevels;
    private LinePrediction line1;
    private LinePrediction line2;
    public override bool StartAttack(Vector2 FirePos, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(FirePos, angle, isflip, actor);
        Bullet tempBullet = Instantiate(bullet, FirePos, Quaternion.Euler(0, 0, attackData[heightLevels].attack1Angle)).GetComponent<Bullet>();
        tempBullet.initialization(actor, isflip);
        Bullet tempBullet2 = Instantiate(bullet, FirePos, Quaternion.Euler(0, 0, attackData[heightLevels].attack2Angle)).GetComponent<Bullet>();
        tempBullet2.initialization(actor, isflip);
        return true;
    }
    public override void ShowLine(Vector2 FirePos, Vector2 dir, Actor actor)
    {
        base.ShowLine(FirePos, dir, actor);
        BoxCollider2D temp = (BoxCollider2D)actor.Collider2D;
        float colliderSizeY = actor.transform.position.y - temp.size.y * 0.5f;
        float playerY = ServiceLocatorManager.GetService<GameManager>().Player.transform.position.y;
        heightLevels = Mathf.FloorToInt((playerY - colliderSizeY) / height);
        if (heightLevels >= attackData.Count)
            heightLevels = attackData.Count - 1;
        line1 =Instantiate(linePrediction);
        line2 = Instantiate(linePrediction);
        line1.Show(FirePos, MathRun.GetAngleToVector2(attackData[heightLevels].attack1Angle), 100);
        line2.Show(FirePos, MathRun.GetAngleToVector2(attackData[heightLevels].attack2Angle), 100);
    }
    public override void HideLine()
    {
        base.HideLine();
        line1.Hide(0);
        line2.Hide(0);
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.color = Color.blue;
        RaycastHit2D hit1 =  Physics2D.Raycast(firePosTest.position, MathRun.GetAngleToVector2(attackData[debugRayNumber].attack1Angle), 100,1<<4);
        Gizmos.DrawLine(firePosTest.position, hit1.point);

        Gizmos.color = Color.red;
        RaycastHit2D hit2 = Physics2D.Raycast(firePosTest.position, MathRun.GetAngleToVector2(attackData[debugRayNumber].attack2Angle), 100,1<<4);
        Gizmos.DrawLine(firePosTest.position, hit2.point);

        Gizmos.color = Color.green;
        BoxCollider2D temp = (BoxCollider2D)owner.Collider2D;
        float colliderSizeY = temp.size.y*0.5f;
        float center1 = owner.transform.position.y - colliderSizeY + height * 0.5f;
        Gizmos.DrawWireCube(new Vector2(owner.transform.position.x, center1), new Vector2(40, height));
        float center2 = center1 + height;
        Gizmos.DrawWireCube(new Vector2(owner.transform.position.x, center2), new Vector2(40, height));
        float center3 = center2 + height;
        Gizmos.DrawWireCube(new Vector2(owner.transform.position.x, center3), new Vector2(40, height));
    }
}
