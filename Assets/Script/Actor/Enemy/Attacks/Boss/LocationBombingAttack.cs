using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using ProjectRun.Framwork.Utility;

public class LocationBombingAttack : AttackBase
{
    [SerializeField] private Vector2 attackAreaPos;
    [SerializeField] private Vector2 attackAreaSize;
    [SerializeField] private BombingSpawner bombingSpawner;
    [SerializeField] private int areaSliceConut=2;
    [SerializeField] private int comboCount=1;
    [SerializeField]
    [ShowIf("@comboCount>1")]
    private float delayTime=1;
    private Vector2 ownerPos;
    public override bool StartAttack(Vector2 value, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(value, angle, isflip, actor);
        ownerPos = actor.transform.position;
        float areaLeftSide = (ownerPos + attackAreaPos).x - attackAreaSize.x*0.5f;
        float boxSizeCenter = (attackAreaSize.x / (areaSliceConut+1));
        List<Vector2> attackPosList = new List<Vector2>();
        for (int i=0;i< areaSliceConut; i++)
        {
            Vector2 attackPos = new Vector2(areaLeftSide + boxSizeCenter, ownerPos.y);
            areaLeftSide = areaLeftSide + boxSizeCenter;
            attackPosList.Add(attackPos);
        }
        List<BombingSpawner> spawners = new List<BombingSpawner>();
        foreach (Vector2 attackPos in attackPosList)
        {
            spawners.Add(Instantiate(bombingSpawner, attackPos, bombingSpawner.transform.rotation));
        }
        StartCoroutine(MakeGuideLine(spawners));
        return true;
    }
    private IEnumerator MakeGuideLine(List<BombingSpawner> spawners)
    {
        for(int x=0;x< comboCount;x++)
        {
            MathRun.GetShuffleList(spawners);
            for (int i = 0; i < spawners.Count; i++)
            {
                spawners[i].HideLine();
            }
            for (int i = 0; i < spawners.Count; i++)
            {
                spawners[i].MakeGuideLine(x, attackAreaSize.y);
            }
            yield return new WaitForSeconds(delayTime);
        }
        StartCoroutine(MakeBomb(spawners));
    }
    private IEnumerator MakeBomb(List<BombingSpawner> spawners)
    {
        for (int x = 0; x < comboCount; x++)
        {
            for (int i = 0; i < spawners.Count; i++)
            {
                spawners[i].MakeBombStart(x,damage);
            }
            yield return new WaitForSeconds(delayTime);
        }
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.color = Color.white;
        Vector2 newPos = attackAreaPos + (Vector2)transform.position;
        Gizmos.DrawWireCube(newPos, attackAreaSize);
    }
}
