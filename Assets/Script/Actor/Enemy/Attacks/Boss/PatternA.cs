using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatternA : AttackBase
{
    [SerializeField] private LinePrediction line;
    private LinePrediction showedLine;
    [SerializeField] private Transform testFirePos;
    [SerializeField] private Vector2 colliderSize = new Vector2(30, 4);
    public override bool StartAttack(Vector2 FirePos, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(FirePos, angle, isflip, actor);
        Collider2D hitCollider = Physics2D.OverlapBox(FirePos, colliderSize, 0, 1 << 7);
        Debug.Log(FirePos);
        Debug.Log(hitCollider.name);
        return true;
    }
    public override void HideLine()
    {
        base.HideLine();
        showedLine?.Hide(0);
    }
    public override void ShowLine(Vector2 FirePos, Vector2 dir, Actor owner)
    {
        base.ShowLine(FirePos, dir, owner);
        showedLine = Instantiate(line);
        showedLine.Show(FirePos, dir, 30);
        showedLine.SetLineWidth(0.5f, 0.5f);
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.DrawCube(testFirePos.position, colliderSize);
    }
}
