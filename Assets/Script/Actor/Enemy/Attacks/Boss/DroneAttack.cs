using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectRun.Framwork.Utility;
using System;
using Sirenix.OdinInspector;

public class DroneAttack : AttackBase
{
    [HideInInspector] public Action OnDroneAttackComplite;
    [SerializeField] private GameObject drone;
    [SerializeField] private List<Transform> dronePos = new List<Transform>();
    [SerializeField] private float delayTime;
    [SerializeField]
    [Tooltip("드론이 가이드라인 생성하는 딜레이 타임")]
    private float lineDelayTime;
    [SerializeField] private float droneMoveSpeed=2;
    [SerializeField]
    [Tooltip("모든공격 완료 후 드론삭제까지의 딜레이 타임")]
    private float destroyDroneTime;
    private Vector2 droneSpawnPos;

    #region test
    [SerializeField]
    [FoldoutGroup("Test")]
    private Transform firePosTest;
    [SerializeField]
    [FoldoutGroup("Test")]
    private float height;
    [SerializeField]
    [FoldoutGroup("Test")]
    private Actor owner;
    private int heightLevels;
    #endregion
    public override bool StartAttack(Vector2 FirePos, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(FirePos, angle, isflip, actor);
        List<DroneAttached> drones = new List<DroneAttached>();
        droneSpawnPos = new Vector2(dronePos[0].position.x, actor.transform.position.y + 15);
        for (int i=0;i< dronePos.Count;i++)
        {
            DroneAttached DroneTemp = Instantiate(drone, droneSpawnPos, drone.transform.rotation).GetComponent<DroneAttached>();
            drones.Add(DroneTemp);
        }
        for(int i=0;i< drones.Count;i++)
        {
            DroneAttached nextDrone = null;
            float nextDroneDis = 0;
            if (i != drones.Count - 1)
            {
                nextDroneDis = Vector2.Distance(dronePos[i].transform.position, dronePos[i+1].transform.position);
                nextDrone = drones[i + 1];
            }
            drones[i].OnSetUp += () =>
            {
                if (SetupCompliteCheck(drones))
                { 
                    StartCoroutine(MakeDronLine(drones, MakeAttackSequence(actor)));
                };
            };
            drones[i].Spwn(dronePos[i].position, nextDroneDis, droneMoveSpeed, nextDrone);
        }
        drones[0].MoveStart();
        OnDroneAttackComplite += () =>
        {
            StartCoroutine(CoolTimer(()=>
            {
                DestroyDrone(drones);
            }, destroyDroneTime));
        };
        return true;
    }
    private IEnumerator MakeDronLine(List<DroneAttached> drones,List<int> attackSequence)
    {
        for(int i=0;i< attackSequence.Count;i++)
        {
            drones[attackSequence[i]].ShowLine();
            yield return new WaitForSeconds(lineDelayTime);
        }
        StartCoroutine(AttackDrone(drones, attackSequence));
    }
    private IEnumerator AttackDrone(List<DroneAttached> drones, List<int> attackSequence)
    {
        foreach(DroneAttached drone in drones)
        {
            drone.HideLine();
        }
        for (int i = 0; i < attackSequence.Count; i++)
        {
            DroneAttached temp = drones[attackSequence[i]];
            temp.AttackDrone(EAttackType.X);
            if (i == attackSequence.Count - 1)
                OnDroneAttackComplite?.Invoke();
            yield return new WaitForSeconds(delayTime);
        }
    }
    private bool SetupCompliteCheck(List<DroneAttached> drones)
    {
        foreach(DroneAttached drone in drones)
        {
            if (!drone.isSetUP)
                return false;
        }
        return true;
    }
    private List<int> MakeAttackSequence(Actor actor)
    {
        List<int> AttackSequence = new List<int>();
        BoxCollider2D temp = (BoxCollider2D)actor.Collider2D;
        float colliderSizeY = actor.transform.position.y - temp.size.y * 0.5f;
        float playerY = ServiceLocatorManager.GetService<GameManager>().Player.transform.position.y;
        heightLevels = Mathf.FloorToInt((playerY - colliderSizeY) / height);
        if (heightLevels >= 2)
            heightLevels = 2;
        AttackSequence.Add(heightLevels);
        if (heightLevels == 0)
        {
            AttackSequence.Add(UnityEngine.Random.Range(1, 3)); 
            AttackSequence.Add(3 - AttackSequence[1]); 
        }
        else if (heightLevels == 1)
        {
            int[] possibleValues = { 0, 2 };
            int randomIndex = UnityEngine.Random.Range(0, possibleValues.Length);

            AttackSequence.Add(possibleValues[randomIndex]);
            AttackSequence.Add(possibleValues[1 - randomIndex]); 
        }
        else if (heightLevels == 2)
        {
            int[] possibleValues = { 0, 1 };
            int randomIndex = UnityEngine.Random.Range(0, possibleValues.Length);

            AttackSequence.Add(possibleValues[randomIndex]);
            AttackSequence.Add(possibleValues[1 - randomIndex]); 
        }
        return AttackSequence;
    }
    public void DestroyDrone(List<DroneAttached> drones)
    {
        for(int i=0;i< drones.Count;i++)
        {
            Destroy(drones[i].gameObject);
        }
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.color = Color.green;
        BoxCollider2D temp = (BoxCollider2D)owner.Collider2D;
        float colliderSizeY = temp.size.y * 0.5f;
        float center1 = owner.transform.position.y - colliderSizeY + height * 0.5f;
        Gizmos.DrawWireCube(new Vector2(owner.transform.position.x, center1), new Vector2(40, height));
        float center2 = center1 + height;
        Gizmos.DrawWireCube(new Vector2(owner.transform.position.x, center2), new Vector2(40, height));
        float center3 = center2 + height;
        Gizmos.DrawWireCube(new Vector2(owner.transform.position.x, center3), new Vector2(40, height));
    }
}
