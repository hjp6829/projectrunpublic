using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using static UnityEngine.GraphicsBuffer;

public class MainGunAttack : AttackBase
{
    [SerializeField] private LinePrediction line;
    [SerializeField] private float fireTime = 1.5f;
    private LinePrediction showedLine;
    [SerializeField]
    [FoldoutGroup("LineWidth")]
    private float startLineWidth=6;
    [SerializeField]
    [FoldoutGroup("LineWidth")]
    private float endLineWidth=6;
    [SerializeField]
    [FoldoutGroup("Overlap")]
    private Vector2 overlapRightButtom;
    [SerializeField]
    [FoldoutGroup("Overlap")]
    private Vector2 overlapLeftUp;
    [Tooltip("debug그리는 용도로만 사용")]
    [SerializeField]
    [FoldoutGroup("Overlap")]
    private Transform TestoverlapFirePos;
    private Coroutine mainGunCoroutine;
    public override bool StartAttack(Vector2 FirePos, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(FirePos, angle, isflip, actor);
        LinePrediction lineTmep = Instantiate(line);
        Vector2 firedir= isflip?Vector2.left:Vector2.right;
        lineTmep.Show(FirePos, firedir, 30);
        lineTmep.SetLineWidth(startLineWidth, endLineWidth);
        lineTmep.Hide(fireTime);
        Vector2 BottomRight=new Vector2(FirePos.x+overlapRightButtom.x, FirePos.y+ overlapRightButtom.y);
        Vector2 UpLeft = new Vector2(FirePos.x + overlapLeftUp.x, FirePos.y + overlapLeftUp.y);
        mainGunCoroutine = StartCoroutine(HitCoroutine(BottomRight, UpLeft));
        return true;
    }
    public override void SpineEndAttack(Actor actor)
    {
        base.SpineEndAttack(actor);
        StopCoroutine(mainGunCoroutine);
    }
    IEnumerator HitCoroutine(Vector2 bottomRight,Vector2 upLeft)
    {
        while(true)
        {
            Collider2D overlap = Physics2D.OverlapArea(bottomRight, upLeft, 1 << 7);
            if (overlap != null)
            {
                ExecuteEvents.Execute<IDamageable>(overlap.gameObject, null, (a, b) => a.Hit(damage,transform.position));
            }
            yield return new WaitForSeconds(0.1f);
        }
    }
    public override void ShowLine(Vector2 FirePos, Vector2 dir, Actor owner)
    {
        base.ShowLine(FirePos, dir, owner);
        showedLine = Instantiate(line);
        showedLine.Show(FirePos, dir, 30);
    }
    public override void HideLine()
    {
        base.HideLine();
        showedLine.Hide(0);
    }
    private void OnDrawGizmos()
    {
        if (!isDebug)
            return;
        Gizmos.color = Color.gray;
        Vector2 BottomRight = new Vector2(TestoverlapFirePos.position.x + overlapRightButtom.x, TestoverlapFirePos.position.y + overlapRightButtom.y);
        Vector2 UpLeft = new Vector2(TestoverlapFirePos.position.x + overlapLeftUp.x, TestoverlapFirePos.position.y + overlapLeftUp.y);
        Vector2 center = (BottomRight + UpLeft) * 0.5f;
        Vector2 size = new Vector2(UpLeft.x-BottomRight.x, UpLeft.y - BottomRight.y);
        Gizmos.DrawWireCube(center, size);
    }
}
