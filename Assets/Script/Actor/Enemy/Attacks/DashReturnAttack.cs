using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashReturnAttack : AttackBase
{
    [SerializeField] private float dashSpeed;
    [SerializeField]
    [Tooltip("처음 위치에서 종료위치까지의 거리 이걸로 판단")]
    private float dashDis;
    private Actor owner;
    private Vector2 ResetPos;
    public override bool StartAttack(Vector2 FirePos, float angle, bool isflip, Actor actor)
    {
        base.StartAttack(FirePos, angle, isflip, actor);
        owner = actor;
        ResetPos=owner.transform.position;
        EnemyMoveComponent enemyMoveComp = actor.FindComponent<EnemyMoveComponent>();
        enemyMoveComp.Walk.DashStart(dashSpeed, actor.GetBody().transform.localScale.x);
        StartCoroutine(DashEndCheck());
        return true;
    }
    public override void SpineEndAttack(Actor actor)
    {
        base.SpineEndAttack(actor);
        ResetPos = owner.transform.position;
        EnemyMoveComponent enemyMoveComp = actor.FindComponent<EnemyMoveComponent>();
        enemyMoveComp.Walk.DashStart(-dashSpeed, actor.GetBody().transform.localScale.x);
        StartCoroutine(DashEndCheck());
    }
    private IEnumerator DashEndCheck()
    {
        while (true)
        {
            if (Vector2.Distance(owner.transform.position, ResetPos) >= dashDis)
            {
                EnemyMoveComponent enemyMoveComp = owner.FindComponent<EnemyMoveComponent>();
                enemyMoveComp.Walk.DashEnd();
                enemyMoveComp.Walk.IsDash = false;
                break;
            }
            yield return null;
        }
    }
}
