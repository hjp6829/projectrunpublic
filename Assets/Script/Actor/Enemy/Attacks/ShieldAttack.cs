using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldAttack : AttackBase
{
    public override bool StartAttack(Vector2 value, float angle, bool isflip, Actor actor)
    {
        if (!base.StartAttack(value, angle, isflip, actor))
            return false;
        return true;
    }
}
