using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Sirenix.OdinInspector;
public class Actor : Entity
{
    [HideInInspector] public Action<LayerMask> OnCollderEnter;
    [HideInInspector] public Action<LayerMask> OnCollderExit;
    /// <summary>
    /// 2023.06.28
    /// 나중에 스테이터스관리 컴포넌트로 옮겨야함
    /// </summary>

    #region 인스펙터
    [SerializeField]
    [BoxGroup("Collide")]
    [InlineButton("NewRigidbody", "New")]
    private Rigidbody2D _rigidbody;
    private void NewRigidbody() => Pasing<Rigidbody2D>(gameObject);
    [SerializeField]
    [BoxGroup("Collide")]
    private Collider2D _collider;
    private const string _bodyName = "Body";
    private GameObject body;

    public Collider2D Collider2D { get => _collider; } 
    public Rigidbody2D Rigidbody { get => _rigidbody; set => _rigidbody = value; }
    public GameObject GetBody()
    {
        if (body != null)
            return body;
        try
        {
            body = transform.Find(_bodyName).gameObject;
        }
        catch (NullReferenceException)
        {
            if (body == null)
            {
                body = NewChild(_bodyName);
            }
        }
        return body;
    }
    private T Pasing<T>(GameObject body)
         where T : Component
    {
        if (body == null)
        {
            throw new ArgumentNullException();
        }
        var temp = body.GetComponent<T>();
        if (temp == null)
        {
            temp = body.AddComponent<T>();
        }
        return temp;
    }
    public GameObject NewChild(string name)
    {
        var newObject = new GameObject();

        newObject.name = name;
        newObject.transform.SetParent(transform);
        newObject.transform.localPosition = new Vector3(0.0f, 0.0f, 0.0f);

        return newObject;
    }
    #endregion
    protected virtual void OnTriggerExit2D(Collider2D collision)
    {
        OnCollderExit?.Invoke(collision.gameObject.layer);
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        OnCollderEnter?.Invoke(collision.gameObject.layer);
    }
}
