using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using static UnityEngine.UI.CanvasScaler;

public class StatusCompoent : ComponentBase, IDamageable
{
    [HideInInspector] public Action OnDie;
    [HideInInspector] public Action<float,float,float> OnHPChange;
    [HideInInspector] public Action OnGroggy;
    [HideInInspector] public Action<string,bool> OnStatusName;
    [HideInInspector] public Action OnHit;
    #region 스테이터스

    [SerializeField]
    [Tooltip("나중에 maxhp불러와서 초기화 할 예정")]
    protected int HP = 5;
    [SerializeField] protected int maxHP = 5;
    [SerializeField] protected int Shield = 5;
    [SerializeField]
    [Tooltip("-1이면 애니 끝나면 바로 무적X\n hit애니 시작부터 작동")]
    private float hitInvincibilityTime = -1;

    protected bool isDied;
    protected bool isHit;

    public bool IsDied { get => isDied; }
    #endregion

    private void Awake()
    {
        Owner.AddComponent(this);
    }
    private void Start()
    {
        maxHP = HP;
        SpineComponent spineComp = Owner.FindComponent<SpineComponent>();
        spineComp.OnHitStart += () =>
        {
            isHit = true;
            if (hitInvincibilityTime > 0)
                activeCoroutine(() =>
                {
                    isHit = false;
                }, hitInvincibilityTime);
        }; 
        spineComp.OnHitComplite += () =>
        {
            if(hitInvincibilityTime<0)
                isHit = false;
        };
        OnHPChange?.Invoke(maxHP, HP, Shield);
    }
    public virtual void Hit(int damage,Vector3 hitPos){}
    public virtual void Hit(int damage) { }
}
