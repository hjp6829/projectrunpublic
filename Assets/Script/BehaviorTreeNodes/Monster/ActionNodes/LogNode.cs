using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogNode : ActionNode
{
    public string log;

    protected override void OnStart()
    {
        action = () =>
        {
            Debug.Log(log);
        };
    }

    protected override void OnStop()
    {
        
    }
}
