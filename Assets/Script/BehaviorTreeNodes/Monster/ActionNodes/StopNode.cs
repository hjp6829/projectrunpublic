using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StopNode : ActionNode
{
    protected override void OnStart()
    {

    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        blackboard.AI.StopEnemy();
        return NodeState.SUCCESS;
    }
}
