using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChaseTargetNode : ActionNode
{
    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {
      
    }
    protected override NodeState OnUpdate()
    {
        blackboard.AI.ChaseTargetPos(blackboard.Target.transform.position);
        return NodeState.RUNNING;
    }
}
