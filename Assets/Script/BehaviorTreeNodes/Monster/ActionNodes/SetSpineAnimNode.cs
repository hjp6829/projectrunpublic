using System.Collections;
using System.Collections.Generic;
using UnityEditor.Experimental.GraphView;
using UnityEngine;

public class SetSpineAnimNode : ActionNode
{
    public string animName;
    public bool Loof;
    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        SpineComponent spineComp = blackboard.Owner.FindComponent<SpineComponent>();
        spineComp.SetSpineAnim(animName, Loof);
        return NodeState.SUCCESS;
    }
}
