using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhaseChangeActionNode : ActionNode
{
    public int phaseNumber;
    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        blackboard.AI.OnPhaseSet(phaseNumber);
        return NodeState.SUCCESS;
    }
}
