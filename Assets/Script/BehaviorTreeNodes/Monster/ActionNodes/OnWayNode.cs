using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectRun.Framwork.Utility;
public class OnWayNode : ActionNode
{
    public EOneWayDir dir;
    protected override void OnStart()
    {

    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        blackboard.AI.OneWayInput(dir);
        return NodeState.RUNNING;
    }
}
