using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class DeadMonsterNode : ActionNode
{
    protected override NodeState OnUpdate()
    {
        return NodeState.FAILURE;
    }
    protected override void OnStart()
    {
       
    }

    protected override void OnStop()
    {
       
    }
}
