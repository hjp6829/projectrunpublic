using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ProjectRun.Framwork.Utility;
using Sirenix.OdinInspector;
public class MonsterAttackNode : ActionNode
{
    public bool isNormal=true;
    [ShowIf("@isNormal==true")]
    public EAttackType attackType;
    [ShowIf("@isNormal!=true")]
    public List<EAttackType> attackTypes;
    private EAttackType beforeAttack;
    private List<EAttackType> possibleAttacks = new List<EAttackType>();
    private bool isRandomStartAttack=true;
    protected override void OnStart()
    {
    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        if (isNormal|| isRandomStartAttack)
        {
            if (isRandomStartAttack)
                blackboard.AI.AttackPlayer(EAttackType.A);
            else
                blackboard.AI.AttackPlayer(attackType);
            isRandomStartAttack = false;
            beforeAttack= attackType;
            return NodeState.SUCCESS;
        }
        possibleAttacks.Clear();
        foreach (EAttackType attackType in attackTypes)
        {
            if (attackType == beforeAttack)
                continue;
            possibleAttacks.Add(attackType);
        }
        int random = Random.Range(0, possibleAttacks.Count);
        EAttackType typeTemp = possibleAttacks[random];

        blackboard.AI.AttackPlayer(typeTemp);
        beforeAttack = typeTemp;
        Debug.Log(typeTemp);
        return NodeState.SUCCESS;

    }
}
