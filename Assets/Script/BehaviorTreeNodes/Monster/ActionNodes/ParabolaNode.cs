using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaNode : ActionNode
{
    private Vector2 startPosition;
    private Transform targetPosition;
    private float elapsedTime = 0.0f;
    private const float gravity = 9.8f;
    private Vector2 velocity;
    public float timeToTarget = 2.0f;
    protected override void OnStart()
    {
        startPosition = blackboard.Owner.transform.position;
        targetPosition = blackboard.targetPosition;
        timeToTarget = blackboard.timeToTarget;
        velocity = ((Vector2)targetPosition.position - startPosition) / timeToTarget;
        velocity.y = (targetPosition.position.y - startPosition.y + 0.5f * gravity * Mathf.Pow(timeToTarget, 2)) / timeToTarget;
    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        elapsedTime += Time.deltaTime;

        // 경과 시간이 전체 이동시간 이내면 위치를 업데이트, 아니라면 목표 위치로 위치 설정
        if (elapsedTime <= timeToTarget)
        {
            float t = elapsedTime;
            blackboard.Owner.transform.position = new Vector2(
                startPosition.x + velocity.x * t,
                startPosition.y + velocity.y * t - 0.5f * gravity * Mathf.Pow(t, 2));
            return NodeState.RUNNING;
        }
        else
        {
            blackboard.Owner.transform.position = targetPosition.position;
            blackboard.IsParabola = false;
            return NodeState.SUCCESS;
        }
    }
}
