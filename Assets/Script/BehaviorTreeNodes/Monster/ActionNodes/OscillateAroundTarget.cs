using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class OscillateAroundTarget : ActionNode
{
    public float PatrolLength=5;
    protected override void OnStart()
    {
      
    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        blackboard.AI.OscillateAroundTarget(PatrolLength);
        return NodeState.SUCCESS;
    }
}
