using System;
using UnityEngine;

public class SetPrefixNode : ActionNode
{
    public string Prefix;
    [HideInInspector] private Action OnPrefix;
    protected override void OnStart()
    {
        SpineComponent spineComp = blackboard.Owner.FindComponent<SpineComponent>();
        OnPrefix += () =>
        {
            spineComp.SetPrefix(Prefix);
        };
    }

    protected override void OnStop()
    {

    }
    protected override NodeState OnUpdate()
    {
        OnPrefix?.Invoke();
        return NodeState.SUCCESS;
    }
}
