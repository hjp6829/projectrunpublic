using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckTargetHeight : Decorator
{
    public float height;
    public CheckTargetHeight(Node childNode) : base(childNode) { }
    protected override void OnStart()
    {
    
    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        float temp = blackboard.Target.transform.position.y - blackboard.Owner.transform.position.y;
        Mathf.Abs(temp);
        if(temp > height)
            return NodeState.SUCCESS;
        return NodeState.FAILURE;
    }
}
