using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckTargetDis : Decorator
{
    public float Dis;
    public CheckTargetDis(Node childNode) : base(childNode)
    {
        this.childNode = childNode;
    }
    protected override void OnStart()
    {

    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        if (blackboard.Target == null)
        {
            this.childNode?.Evaluate();
            return NodeState.SUCCESS;
        }
        Vector2 targetPos = new Vector2(blackboard.Target.position.x, blackboard.Owner.transform.position.y);
        if (Vector2.Distance(blackboard.Owner.transform.position, targetPos) <= Dis)
        {
            this.childNode?.Evaluate();
            return NodeState.SUCCESS;
        }
        return NodeState.FAILURE;
    }
}
