using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseForEventCheck : Decorator
{
    public PauseForEventCheck(Node childNode) : base(childNode)
    {
    }

    protected override void OnStart()
    {

    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        if (blackboard.IsPause)
            return NodeState.SUCCESS;
        if (childNode != null)
        {
            NodeState childState = childNode.Evaluate();
            return childState;
        }
        return NodeState.FAILURE;
    }
}
