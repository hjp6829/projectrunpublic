using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanAttackNode : Decorator
{
    public CanAttackNode(Node Chiled) : base(Chiled) { }
    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {
        
    }

    protected override NodeState OnUpdate()
    {
        if (blackboard.IsAttack)
            return NodeState.FAILURE;
        childNode?.Evaluate();
        return NodeState.SUCCESS;
    }
}
