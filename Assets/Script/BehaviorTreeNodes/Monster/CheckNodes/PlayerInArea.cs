using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInArea : Decorator
{
    public PlayerInArea(Node childNode) : base(childNode) { }
    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        if (blackboard.IsInArea)
        {
            if (childNode != null)
            {
                NodeState childState = childNode.Evaluate();
                return childState;
            }
            return NodeState.SUCCESS;
        }
        else
            return NodeState.FAILURE;
    }
}
