using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaCheck : Decorator
{
    public ParabolaCheck(Node childNode) : base(childNode)
    {
    }

    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        if (!blackboard.IsParabola)
            return NodeState.FAILURE;
        if (childNode != null)
        {
            NodeState childState = childNode.Evaluate();

            return childState;
        }
        return NodeState.SUCCESS;
    }
}
