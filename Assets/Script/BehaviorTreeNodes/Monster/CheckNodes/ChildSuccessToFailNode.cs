using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChildSuccessToFailNode : Decorator
{
    private bool isOnceCheck=false;
    public ChildSuccessToFailNode(Node childNode) : base(childNode)
    {
        this.childNode = childNode;
    }
    protected override void OnStart()
    {

    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        NodeState state = childNode.Evaluate();
        if (!isOnceCheck&& state == NodeState.SUCCESS)
        { 
            isOnceCheck = true;
            return state;
        }
        return NodeState.FAILURE;
    }
}
