using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CanMoveNode : Decorator
{
    public CanMoveNode(Node Chiled) : base(Chiled) { }
    protected override void OnStart()
    {

    }

    protected override void OnStop()
    {

    }

    protected override NodeState OnUpdate()
    {
        if (blackboard.IsHit)
            return NodeState.FAILURE;
        childNode?.Evaluate();
        return NodeState.SUCCESS;
    }
}
