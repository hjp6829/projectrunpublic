using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerInRangeNode : Decorator
{
    private Transform targetPos;
    private Transform ownerPos;
    public float Range;
    public PlayerInRangeNode(Node childNode):base(childNode) {  }

    protected override void OnStart()
    {
        targetPos = blackboard.Target.transform;
        ownerPos = blackboard.Owner.transform;
    }

    protected override void OnStop()
    {
        
    }

    protected override NodeState OnUpdate()
    {
        float dis=Vector2.Distance(targetPos.position, ownerPos.position);
        if (dis < Range)
        {
            if(childNode!=null)
            {
                NodeState childState = childNode.Evaluate();

                return childState;
            }
            return NodeState.SUCCESS;
        }

        return NodeState.FAILURE;
    }
}