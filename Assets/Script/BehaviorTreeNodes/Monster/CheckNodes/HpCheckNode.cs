using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class CheckMonsterHP : Decorator
{
    [Range(0,100)]
    public int minHP;
    public CheckMonsterHP(Node childNode) : base(childNode)
    {
        this.childNode = childNode;
    }

    protected override void OnStart()
    {
      
    }

    protected override void OnStop()
    {
       
    }

    protected override NodeState OnUpdate()
    {
       float temp = blackboard.HP / blackboard.MaxHP;
        if (temp <= minHP * 0.01f)
        {
            childNode?.Evaluate();
            return NodeState.SUCCESS;
        }
        else if (temp > minHP * 0.01f)
            return NodeState.FAILURE;
        return NodeState.SUCCESS;
    }
}
