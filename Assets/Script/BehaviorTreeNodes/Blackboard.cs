using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
[System.Serializable]
public class Blackboard
{ 
    public int MaxHP;
    public int HP;
    [HideInInspector] public Actor Owner;
    [HideInInspector] public Transform Target;
    [HideInInspector] public AIController AI;
    [HideInInspector] public bool IsInArea;
    [HideInInspector] public bool IsAttack;
    [HideInInspector] public bool IsHit;
    [HideInInspector] public bool IsFallInFront;
    [HideInInspector] public bool IsParabola;
    [HideInInspector] public bool IsGroggy;
   public bool IsPause;
    #region Parabola
    [HideInInspector] public Transform targetPosition;
    [HideInInspector] public float timeToTarget = 2.0f;
    #endregion
}
