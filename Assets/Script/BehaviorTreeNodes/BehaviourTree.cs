using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[CreateAssetMenu()]
public class BehaviourTree : ScriptableObject
{
    
    public Node rootNode;
    public Node.NodeState treeState = Node.NodeState.RUNNING;
    public List<Node> nodes = new List<Node>();
    public Blackboard blackboard = new Blackboard();

    public Node.NodeState Update()
    {
           treeState = rootNode.Evaluate();
        return treeState;
    }

    public Node CreateNode(System.Type type)
    {
        Node node = ScriptableObject.CreateInstance(type) as Node;
        node.name = type.Name;
        node.guid = GUID.Generate().ToString();
        nodes.Add(node);
        if(!Application.isPlaying)
        {
            AssetDatabase.AddObjectToAsset(node, this);
        }

        AssetDatabase.SaveAssets();
        return node;
    }
    public void DeleteNode(Node node)
    {
        nodes.Remove(node);
        AssetDatabase.RemoveObjectFromAsset(node);
        AssetDatabase.SaveAssets();
    }
    public void AddChild(Node parent, Node child)
    {
        Decorator decorator = parent as Decorator;
        if (decorator)
        {
            Undo.RecordObject(decorator, "Behaviour Tree (AddChild");
            decorator.ChildNode = child;
            EditorUtility.SetDirty(decorator);
        }
        RootNode rootNode = parent as RootNode;
        if (rootNode)
        {
            Undo.RecordObject(rootNode, "Behaviour Tree (AddChild");
            rootNode.child = child;
            EditorUtility.SetDirty(rootNode);
        }
        Composite composite = parent as Composite;
        if (composite)
        {
            Undo.RecordObject(composite, "Behaviour Tree (AddChild");
            composite.Children.Add(child);
            EditorUtility.SetDirty(composite);
        }
    }
    public void RemoveChild(Node parent, Node child)
    {
        Decorator decorator = parent as Decorator;
        if (decorator)
        {
            Undo.RecordObject(decorator, "Behaviour Tree (RemoveChild");
            decorator.ChildNode = null;
            EditorUtility.SetDirty(decorator);
        }
        Composite composite = parent as Composite;
        if (composite)
        {
            Undo.RecordObject(composite, "Behaviour Tree (RemoveChild");
            composite.Children.Remove(child); 
            EditorUtility.SetDirty(composite);
        }
        RootNode rootNode = parent as RootNode;
        if (rootNode)
        {
            Undo.RecordObject(rootNode, "Behaviour Tree (RemoveChild");
            rootNode.child = null;
            EditorUtility.SetDirty(rootNode);
        }
    }
    public List<Node> GetChildren(Node parent)
    {
        List<Node> children = new List<Node>();
        switch (parent)
        {
            case Decorator decorator:
                if (decorator.ChildNode != null)
                    children.Add(decorator.ChildNode);
                break;
            case Composite composite:
                return composite.Children;
            case RootNode rootNode:
                if (rootNode.child != null)
                    children.Add(rootNode.child);
                break;
        }
        return children;
    }
    public BehaviourTree Clone()
    {
        BehaviourTree tree = Instantiate(this);
        tree.rootNode = tree.rootNode.Clone();
        tree.nodes = new List<Node>();
        Traverse(tree.rootNode, (n) =>
        {
            tree.nodes.Add(n);
        });
        return tree;
    }
    public void Traverse(Node node,System.Action<Node> visiter)
    {
        if(node)
        {
            visiter.Invoke(node);
            var children=GetChildren(node);
            children.ForEach((n) => Traverse(n, visiter));
        }
    }
    public void Bind(Actor owner,AIController ai)
    {
        blackboard.Owner = owner;
        blackboard.AI = ai;
        blackboard.Target = ai.Player.transform;
        Traverse(rootNode, node =>
        {
            node.blackboard = blackboard;
        });
    }
}
