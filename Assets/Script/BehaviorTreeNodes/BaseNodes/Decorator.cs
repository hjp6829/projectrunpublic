using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Decorator : Node
{
    [SerializeField]
    protected Node childNode;

    public Node ChildNode { get => childNode; set => childNode = value; }

    public Decorator(Node childNode)
    {
        this.childNode = childNode;
    }
    public override Node Clone()
    {
        Decorator node = Instantiate(this);
        if (node.ChildNode!=null)
            node.ChildNode = childNode.Clone();
        return node;
    }
}
