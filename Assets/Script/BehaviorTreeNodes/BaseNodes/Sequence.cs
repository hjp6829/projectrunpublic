using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sequence : Composite
{
    private int lastRunningNodeIndex;
    public Sequence(List<Node> nodes)
    {
        this.nodes = nodes;
    }

    protected override void OnStart()
    {
        lastRunningNodeIndex = 0;
    }

    protected override void OnStop()
    {
      
    }

    protected override NodeState OnUpdate()
    {
        for (int i = lastRunningNodeIndex; i < nodes.Count; i++)
        {
            switch (nodes[i].Evaluate())
            {
                case NodeState.SUCCESS:
                    continue;
                case NodeState.FAILURE:
                    lastRunningNodeIndex = 0;
                    return NodeState.FAILURE;
                case NodeState.RUNNING:
                    lastRunningNodeIndex = i;
                    return NodeState.RUNNING;
                default:
                    continue;
            }
        }

        lastRunningNodeIndex = 0;
        return NodeState.SUCCESS;
    }
}
