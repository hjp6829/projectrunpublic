using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeaterNode : Decorator
{
    public int repeatCount;
    private int currentCount;

    public RepeaterNode(Node childNode):base(childNode)
    {
        this.currentCount = 0;
    }

    protected override void OnStart()
    {
        currentCount = 0;
    }

    protected override void OnStop()
    {
       
    }

    protected override NodeState OnUpdate()
    {
        if (currentCount >= repeatCount && repeatCount != -1)
        {
            return NodeState.SUCCESS;
        }

        NodeState childState = childNode.Evaluate();

        if (childState == NodeState.SUCCESS || childState == NodeState.FAILURE)
        {
            currentCount++;
        }

        return NodeState.RUNNING;
    }
}
