using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RootNode : Node
{
    public Node child;
    protected override NodeState OnUpdate()
    {
        return child.Evaluate();
    }
    public override Node Clone()
    {
        RootNode node = Instantiate(this);
        node.child = child.Clone();
        return node;
    }

    protected override void OnStart()
    {
        
    }

    protected override void OnStop()
    {
       
    }
}
