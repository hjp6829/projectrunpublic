using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inverter : Decorator
{
    public Inverter(Node childNode):base(childNode)
    {
        
    }

    protected override NodeState OnUpdate()
    {
        NodeState state = childNode.Evaluate();
        switch (state)
        {
            case NodeState.SUCCESS:
                return NodeState.FAILURE;
            case NodeState.FAILURE:
                return NodeState.SUCCESS;
            case NodeState.RUNNING:
                return NodeState.RUNNING;
        }
        return NodeState.SUCCESS;
    }
    public override Node Clone()
    {
        Inverter node = Instantiate(this);
        node.childNode = childNode.Clone();
        return node;
    }

    protected override void OnStart()
    {
       
    }

    protected override void OnStop()
    {
      
    }
}