using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ActionNode : Node
{
    [SerializeField]
    protected Action action;

    protected override NodeState OnUpdate()
    {
        action?.Invoke();
        return NodeState.SUCCESS;
    }
    public override Node Clone()
    {
        ActionNode node = Instantiate(this);
        return node;
    }
}
