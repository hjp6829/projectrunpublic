using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public abstract class Node :ScriptableObject
{
    public enum NodeState { RUNNING, SUCCESS, FAILURE }

    [SerializeField]
    protected NodeState currentNodeState;

    [HideInInspector] public bool isstart = false;
    public NodeState state { get => currentNodeState; }
    [HideInInspector] public string guid;
    [HideInInspector] public Vector2 Pos;
    [HideInInspector] public Blackboard blackboard;
    [TextArea] public string description;
    public Node() { }

    public virtual Node Clone()
    {
        return Instantiate(this);
    }
    public NodeState Evaluate()
    {
        if (!isstart)
        {
            OnStart();
            isstart = true;
        }

        currentNodeState = OnUpdate();

        if (currentNodeState != NodeState.RUNNING)
        {
            OnStop();
            isstart = false;
        }

        return state;
    }
    protected abstract void OnStart();
    protected abstract void OnStop();
    protected abstract NodeState OnUpdate();
}




