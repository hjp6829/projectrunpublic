using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Composite : Node
{
    [SerializeField]
    protected List<Node> nodes = new List<Node>();

    public List<Node> Children
    {
        get
        {
            if (nodes == null)
            {
                nodes = new List<Node>();
                return nodes;
            }
            return nodes;
        }
        set => nodes = value;
    }
    public override Node Clone()
    {
        Composite node = Instantiate(this);
        node.Children = Children.ConvertAll(c => c.Clone());
        return node;
    }
}
