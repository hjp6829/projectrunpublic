using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condition : Node
{
    public delegate bool conditionDelegate();
    private conditionDelegate condition;

    public Condition(conditionDelegate condition)
    {
        this.condition = condition;
    }

    protected override NodeState OnUpdate()
    {
        if (condition())
        {
            return NodeState.SUCCESS;
        }
        return NodeState.FAILURE;
    }

    protected override void OnStart()
    {
       
    }

    protected override void OnStop()
    {
        
    }
}
