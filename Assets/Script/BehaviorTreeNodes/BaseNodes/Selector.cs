using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Selector : Composite
{
    public Selector(List<Node> nodes)
    {
        this.nodes = nodes;
    }

    protected override void OnStart()
    {
       
    }

    protected override void OnStop()
    {
        
    }

    protected override NodeState OnUpdate()
    {
        foreach (Node node in nodes)
        {
            switch (node.Evaluate())
            {
                case NodeState.SUCCESS:
                    return NodeState.SUCCESS;
                case NodeState.RUNNING:
                    return NodeState.RUNNING;
                default:
                    continue;
            }
        }
        return NodeState.FAILURE;
    }
}
