using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class test : Tile
{
    public bool isRay;
    public int data;
    public int X;
    public int Y;
    public float HitY;
    public test Clone()
    {
        test newtemp = (test)CreateInstance("test");// new test();
        newtemp.data = data;
        return newtemp;
    }
    public Vector2 GetPos()
    {
        return new Vector2(X, Y);
    }
}
