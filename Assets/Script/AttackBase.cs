using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using System;
using ProjectRun.Framwork.Utility;
using UnityEngine.UI;

[Serializable]
public class ComboData
{
    public EAttackType type;
    public int attacknum;
}
public abstract class AttackBase : SerializedMonoBehaviour
{
    public int idx;
    public bool isDebug;
    [SerializeField]
    protected float coolTime = -1;
    [SerializeField] protected int damage;
    protected bool canAttack=true;
    /// <summary>
    /// 2023.6.12일
    /// 공격시작
    /// </summary>
    public virtual bool StartAttack(Vector2 FirePos, float angle, bool isflip,Actor actor)
    {
        canAttack = false;
        CoolTimeStart();
        return true;
    }
    public virtual void SpineEndAttack(Actor actor) { }
    public virtual void ShowLine(Vector2 FirePos, Vector2 dir,Actor actor) { }
    public virtual void HideLine() { }
    protected void CoolTimeStart()
    {
        StartCoroutine(CoolTimer(() => canAttack = true, coolTime));
    }
    public bool CanAttack()
    {
        return canAttack;
    }
    protected IEnumerator CoolTimer(Action action,float time)
    {
        if (time < 0)
        {
            action?.Invoke();
            yield break;
        }
        float cooltime = time;
        while(true)
        {
            cooltime -= Time.deltaTime;
            if(cooltime < 0)
            {
                action?.Invoke();
                break;
            }
            yield return null;
        }
    }
}
