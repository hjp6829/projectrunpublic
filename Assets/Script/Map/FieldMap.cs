using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldMap : MonoBehaviour
{
    [SerializeField]
    private List<FieldArea> Areas = new List<FieldArea>();
    private FieldArea currentArea;
    [SerializeField]
    private MainCamera mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        GameManager gameManager = ServiceLocatorManager.GetService<GameManager>();
        gameManager.CurrentField = this;
        currentArea = Areas[0];
        currentArea.AreaIDX = 0;
        foreach(var area in Areas)
        {
            //try
            //{
            //    area.NextArea = Areas[++currentArea.AreaIDX];
            //}
            //catch()
        }
        mainCamera.SetAreaSize(currentArea.GetAreaCenter(), currentArea.GetAreaSize());
    }
}
