using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FieldArea : MonoBehaviour
{
    [SerializeField]
    private List<Enemy> Enemys = new List<Enemy>();
    [SerializeField]
    private Vector2 areaSize;
    private FieldArea nextArea;
    private int areaIDX;

    public int AreaIDX { get => areaIDX; set => areaIDX = value; }
    public FieldArea NextArea { set => nextArea = value; }
    // Start is called before the first frame update
    void Start()
    {
        foreach(var enemy in Enemys)
        {
            enemy.FindComponent<StatusCompoent>().OnDie += () =>
            {
                if (CheckClear())
                    ClearField();
            };
        }
    }
    private bool CheckClear()
    {
        foreach(var enemy in Enemys)
        {
            if (!enemy.FindComponent<StatusCompoent>().IsDied)
                return false;
        }
        return true;
    }
    private void ClearField()
    {
        Debug.Log("룸 클리어");
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawWireCube(transform.position, areaSize);
    }
    public Vector2 GetAreaSize() { return areaSize; }
    public Vector2 GetAreaCenter() { return transform.position; }
}
