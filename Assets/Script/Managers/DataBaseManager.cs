using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using Mono.Data.SqliteClient;
using ProjectRun.Framwork.Utility;
using System;

public class DataBaseManager : SerializedMonoBehaviour, IService
{
    private Dictionary<string, SqliteReader.Table> _tables;
    public Dictionary<string, SqliteReader.Table> Tables { get => _tables; }
    //[SerializeField]
    private Dictionary<EAttackType, AttackData> attacks=new Dictionary<EAttackType, AttackData>();
    private void Awake()
    {
        Register();
        SqlLode();
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    private void SqlLode()
    {
        var sql = new SqliteReader();
        sql.Open("Data.db");

        _tables = new Dictionary<string, SqliteReader.Table>();
        foreach (var name in sql.TableList)
        {
            sql.ReadTable = name;
            _tables.Add(name, sql.Read());
        }

        sql.Close();
    }
    public object GetColumn(string pawnName, int idx, string columnName)
    {
        try
        {
            return Tables[pawnName][idx][columnName];
        }
        catch (KeyNotFoundException)
        {
            UnityEngine.Debug.LogError("Monster {ToString()} {" + idx + "} {columnName} not found. : ");
            return 1;
        }
    }
    public Dictionary<EAttackType, AttackData> LoadPlayerCombo()
    {
        List<EAttackType> ComboListtest = new List<EAttackType>();
        List<int> ComboAttacks = new List<int>();
        List<string> ComboNames = new List<string>();
        PlayerAttacks playerAttacks = ServiceLocatorManager.GetService<PlayerAttacks>();
        foreach (var Combo in Tables["Combo"])
        {
            ComboListtest.Clear();
            ComboAttacks.Clear();
            ComboNames.Clear();
            foreach (var ComboName in Combo.Value)
            {
                if(ComboName.Key.Contains("AttackIDX"))
                {
                    ComboAttacks.Add((int)ComboName.Value);
                    continue;
                }
                else if(ComboName.Key.Contains("AttackName"))
                {
                    ComboNames.Add((string)ComboName.Value);
                    continue;
                }
                string temp = (string)ComboName.Value;
                temp = temp.ToUpper();
                EAttackType enumtemp = (EAttackType)Enum.Parse(typeof(EAttackType), temp, true);
                ComboListtest.Add(enumtemp);
            }
            for (int i=0;i<3;i++)
            {
                if (attacks.ContainsKey(ComboListtest[0]))
                {
                    if (attacks[ComboListtest[0]].NextAttack.ContainsKey(ComboListtest[1]))
                    {
                        if (!attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].NextAttack.ContainsKey(ComboListtest[2]))
                        {
                            attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].NextAttack.Add(ComboListtest[2], new AttackData());
                            attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].NextAttack[ComboListtest[2]].attackName = ComboNames[2];
                            attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].NextAttack[ComboListtest[2]].attack = playerAttacks.GetPlayerAttack(ComboAttacks[2]);
                            attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].NextAttack[ComboListtest[2]].isLastCombo = true;
                        }
                    }
                    else
                    {
                        attacks[ComboListtest[0]].NextAttack.Add(ComboListtest[1], new AttackData());
                        attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].attackName = ComboNames[1];
                        attacks[ComboListtest[0]].NextAttack[ComboListtest[1]].attack= playerAttacks.GetPlayerAttack(ComboAttacks[1]);
                    }
                }
                else
                {
                    attacks.Add(ComboListtest[0], new AttackData());
                    attacks[ComboListtest[0]].attackName = ComboNames[0];
                    attacks[ComboListtest[0]].attack = playerAttacks.GetPlayerAttack(ComboAttacks[0]);
                }
            }
        }
        return attacks;
    }
}
