using Mono.Data.SqliteClient;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using UnityEngine;

public class SqliteReader : MonoBehaviour
{
    public class Table : Dictionary<int, Dictionary<string, object>> { }
    private string _readTable = "";
    private IDbConnection _dbConnection = null;
    private List<string> _tableList = null;

    /// <summary>
    /// 읽을 테이블 - 읽고자하는 테이블입니다.
    /// </summary>
    public string ReadTable { get => _readTable; set => _readTable = value; }
    public List<string> TableList { get => _tableList; }

    public void Open(string path)
    {
        path = "URI=file:" + Application.streamingAssetsPath + "/" + path;
        if (IsOpen())
        {
            throw new FileLoadException("이미 파일이 열려 있습니다.");
        }
        _dbConnection = new SqliteConnection(path);
        _dbConnection.Open();
        if (_dbConnection.State != ConnectionState.Open)
        {
            throw new FileLoadException($"{path} 를 찾을수 없습니다.");
        }
        _tableList = GetTableList();
    }

    private IDataReader Commanding(string Command)
    {
        if (!IsOpen())
        {
            throw new FileLoadException("Open()으로 파일을 먼저 열어야 합니다.");
        }
        var command = _dbConnection.CreateCommand();
        command.CommandText = Command;

        var temp = command.ExecuteReader();

        command.Dispose();
        return temp;
    }

    private List<string> GetTableList()
    {
        if (!IsOpen())
        {
            throw new FileLoadException("Open()으로 파일을 먼저 열어야 합니다.");
        }

        var temp = new List<string>();
        var reader = Commanding("SELECT name FROM sqlite_master WHERE type = 'table';");
        while (reader.Read())
        {
            for (int i = 0; i < reader.FieldCount; i++)
            {
                var name = reader.GetString(i);
                if (name != "sqlite_sequence")
                {
                    temp.Add(name);
                }
            }
        }
        return temp;
    }

    public Table Read()
    {
        if (_readTable == "")
        {
            throw new ArgumentNullException("ReadTable의 값을 먼저 설정해야합니다.");
        }
        var reader = Commanding($"Select * From {_readTable}");

        var temp = new Table();

        while (reader.Read())
        {
            int index = -1;
            for (int i = 0; i < reader.FieldCount; i++)
            {
                var value = reader.GetValue(i);
                if (i == 0)
                {
                    if (value is int)
                    {
                        index = (int)value;
                        temp.Add(index, new Dictionary<string, object>());
                    }
                    else // 첫 컬럼이 index가 아니면 해당 테이블 건너뛰기
                    {
                        continue;
                    }
                }
                else
                {
                    if (value is double)
                    {
                        value = Convert.ToSingle(value);
                    }
                    temp[index].Add(reader.GetName(i), value);
                }
            }
        }
        reader.Dispose();

        return temp;
    }
    public void Close()
    {
        _dbConnection.Close();
        _dbConnection = null;
    }

    public bool IsOpen()
    {
        return _dbConnection != null;
    }
}
