using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class GameManager : SerializedMonoBehaviour,IService
{
    private Player player;
    private FieldMap currentField;

    public FieldMap CurrentField { get => currentField; set => currentField = value; }
    public Player Player { get => player; set => player = value; }

    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }

    private void Awake()
    {
        Register();
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }
}
