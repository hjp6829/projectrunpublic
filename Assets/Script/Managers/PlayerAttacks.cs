using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
public class PlayerAttacks : SerializedMonoBehaviour,IService
{
    [SerializeField]
    private Dictionary<int, AttackBase> playerAttackDic = new Dictionary<int, AttackBase>();
    private void Awake()
    {
        Register();
    }
    public void Register()
    {
        ServiceLocatorManager.Register(this);
    }
    public AttackBase GetPlayerAttack(int idx)
    {
        return playerAttackDic[idx];
    }

}
