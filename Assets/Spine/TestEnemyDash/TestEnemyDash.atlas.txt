
TestEnemyDash.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Arm_L_down
  rotate: false
  xy: 2, 122
  size: 27, 44
  orig: 29, 46
  offset: 1, 1
  index: -1
Arm_L_up
  rotate: false
  xy: 31, 144
  size: 10, 22
  orig: 12, 25
  offset: 1, 1
  index: -1
Arm_R_down
  rotate: false
  xy: 24, 34
  size: 14, 22
  orig: 16, 29
  offset: 1, 6
  index: -1
Arm_R_up
  rotate: false
  xy: 31, 124
  size: 11, 18
  orig: 13, 20
  offset: 1, 1
  index: -1
Body_down
  rotate: false
  xy: 30, 84
  size: 16, 16
  orig: 18, 18
  offset: 1, 1
  index: -1
Body_up
  rotate: false
  xy: 2, 54
  size: 20, 27
  orig: 22, 29
  offset: 1, 1
  index: -1
Head
  rotate: false
  xy: 30, 102
  size: 17, 18
  orig: 20, 20
  offset: 1, 1
  index: -1
Leg_L_down
  rotate: true
  xy: 2, 2
  size: 22, 21
  orig: 22, 21
  offset: 0, 0
  index: -1
Leg_L_up
  rotate: false
  xy: 43, 152
  size: 7, 14
  orig: 10, 16
  offset: 2, 1
  index: -1
Leg_R_down
  rotate: false
  xy: 24, 58
  size: 17, 23
  orig: 17, 23
  offset: 0, 0
  index: -1
Leg_R_up
  rotate: true
  xy: 24, 26
  size: 6, 11
  orig: 9, 13
  offset: 2, 1
  index: -1
부스터1
  rotate: false
  xy: 2, 83
  size: 26, 37
  orig: 28, 39
  offset: 1, 1
  index: -1
부스터2
  rotate: false
  xy: 2, 26
  size: 20, 26
  orig: 22, 28
  offset: 1, 1
  index: -1
