
TestEnemyShield.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
Enemy/Body
  rotate: true
  xy: 2, 198
  size: 32, 86
  orig: 36, 88
  offset: 1, 1
  index: -1
Enemy/Body_Damage
  rotate: true
  xy: 145, 195
  size: 35, 85
  orig: 39, 87
  offset: 1, 1
  index: -1
Enemy/Enemy_Shiled
  rotate: true
  xy: 90, 198
  size: 32, 53
  orig: 34, 55
  offset: 1, 1
  index: -1
