using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParabolaTest : MonoBehaviour
{
    public float timeToTarget = 2.0f;
    public Transform targetPosition;           // 목표 위치
    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch (collision.gameObject.layer)
        {
            case 6:
                AIController aI = collision.GetComponent<AIController>();
                aI.SetParabolaData(timeToTarget, targetPosition);
                break;
        }
    }
}
