using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UnityEditor.Callbacks;
using Codice.Client.BaseCommands.BranchExplorer;

public class BehaviourTreeEditor : EditorWindow
{
    [SerializeField]
    private VisualTreeAsset m_VisualTreeAsset = default;

    BehaviourTreeView treeView;
    InspectorView inspectorView;
    IMGUIContainer BlackboardView;

    SerializedObject treeObject;
    SerializedProperty blackboardProperty;
    [MenuItem("BehaviourTreeEditor/Editor ...")]
    public static void OpenWindow()
    {
        BehaviourTreeEditor wnd = GetWindow<BehaviourTreeEditor>();
        wnd.titleContent = new GUIContent("BehaviourTreeEditor");
    }

    [OnOpenAsset]
    public static bool OnOpenAsset(int instanceId,int line)
    {
        if(Selection.activeObject is BehaviourTree)
        {
            OpenWindow();
            return true;
        }
        return false;
    }

    public void CreateGUI()
    {
        // Each editor window contains a root VisualElement object
        VisualElement root = rootVisualElement;

        // Instantiate UXML
        var m_VisualTreeAsset = AssetDatabase.LoadAssetAtPath<VisualTreeAsset>("Assets/Editor/BehaviourTreeEditor.uxml");
        m_VisualTreeAsset.CloneTree(root);

        var styleSheet = AssetDatabase.LoadAssetAtPath<StyleSheet>("Assets/Editor/BehaviourTreeEditor.uss");
        root.styleSheets.Add(styleSheet);

        treeView = root.Q<BehaviourTreeView>();
        inspectorView = root.Q<InspectorView>();
        BlackboardView= root.Q<IMGUIContainer>();
        BlackboardView.onGUIHandler = () =>
        {
            if (treeObject == null)
                return;
            treeObject.Update();
            EditorGUILayout.PropertyField(blackboardProperty);
            treeObject.ApplyModifiedProperties();
        };
        treeView.OnNodeSelected = OnNodeSelectionChange;
        OnSelectionChange();
    }
    //private void OnPlayModeStateChange(PlayModeStateChange obj)
    //{
    //    switch (obj)
    //    {
    //        case PlayModeStateChange.EnteredEditMode:
    //            OnSelectionChange();
    //            break;
    //        case PlayModeStateChange.ExitingEditMode:
    //            break;
    //        case PlayModeStateChange.EnteredPlayMode:
    //            OnSelectionChange();
    //            break;
    //        case PlayModeStateChange.ExitingPlayMode:
    //            break;
    //    }
    //}
    //private void OnEnable()
    //{
    //    EditorApplication.playModeStateChanged -= OnPlayModeStateChange;
    //    EditorApplication.playModeStateChanged += OnPlayModeStateChange;
    //}
    //private void OnDisable()
    //{
    //    EditorApplication.playModeStateChanged -= OnPlayModeStateChange;
    //}
    private void OnSelectionChange()
    {
        BehaviourTree tree = Selection.activeObject as BehaviourTree;
        if (!tree)
        {
            if (Selection.activeGameObject)
            {
                AIController runner = Selection.activeGameObject.GetComponent<AIController>();
                if (runner)
                    tree = runner.Tree;
            }
        }
        //if (Application.isPlaying)
        //{
        //    if (tree)
        //        treeView.PopulateView(tree);
        //}
        //else
        //{

        //}
        if (tree && AssetDatabase.CanOpenAssetInEditor(tree.GetInstanceID()))
        {
            treeView.PopulateView(tree);
        }
        if (tree!=null)
        {
            treeObject = new SerializedObject(tree);
            blackboardProperty = treeObject.FindProperty("blackboard");
        }
    }

    void OnNodeSelectionChange(NodeView node)
    {
        inspectorView.UpdateSelection(node);
    }
    private void OnInspectorUpdate()
    {
        treeView?.UpdateNodeState();
    }
}
